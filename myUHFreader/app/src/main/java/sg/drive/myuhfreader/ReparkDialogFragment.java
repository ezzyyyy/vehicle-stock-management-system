package sg.drive.myuhfreader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;

public class ReparkDialogFragment extends DialogFragment {
    public interface ReparkDialogListener {
        public void reparkOk(DialogFragment dialog);
        public void reparkNOK(DialogFragment dialog);
    }

    ReparkDialogFragment.ReparkDialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if (context instanceof Activity){
            activity = (Activity) context;

            // Verify that the host activity implements the callback interface
            try {
                // Instantiate the NoticeDialogListener so we can send events to the host
                mListener = (ReparkDialogFragment.ReparkDialogListener) activity;
            } catch (ClassCastException e) {
                // The activity doesn't implement the interface, throw exception
                throw new ClassCastException(activity.toString() + " must implement CollectDialogListener");
            }
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String msg = getResources().getString(R.string.confirm_repark);
        builder.setMessage(Html.fromHtml(msg))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.reparkOk(ReparkDialogFragment.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.reparkNOK(ReparkDialogFragment.this);
                    }
                });

        return builder.create();
    }
}

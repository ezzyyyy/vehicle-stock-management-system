package sg.drive.myuhfreader;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CompleteJobActivity extends AppCompatActivity {
    Button btn_done;
    boolean shouldAllowBack = false;
    String loginkey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(checkLogin(savedInstanceState)){
            setContentView(R.layout.activity_complete_job);
            btn_done = (Button) findViewById(R.id.btn_done);
        }else{
            movetoLogin();
        }
    }

    public boolean checkLogin(Bundle savedInstanceState){
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                loginkey = null;
            } else {
                loginkey = extras.getString("loginkey");
            }
        } else {
            loginkey = (String) savedInstanceState.getSerializable("loginkey");
        }

        if(loginkey == null){
            return false;
        }else{
            return true;
        }
    }

    public void movetoLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void backtoScan(View view){
        Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
        intent.putExtra("loginkey", loginkey);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (!shouldAllowBack) {
            //do not allow back button press
        } else {
            super.onBackPressed();
        }
    }
}

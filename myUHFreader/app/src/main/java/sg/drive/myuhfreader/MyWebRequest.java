package sg.drive.myuhfreader;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class MyWebRequest extends AsyncTask<String, Void, String> {
    public AsyncResponse delegate = null;
    private String uri = "http://dev.skyfy.com.sg:5006/fleet.svc/web";
    private String APIKEY;

    //endpoints
    private final static String GETBINS = "/getbins";
    private final static String GETBIN = "/getbin ";
    private final static String UPDATEBINJOB = "/UpdateBinJob";
    private final static String UPDATEJOBLOCATION = "/SaveJobLocation";
    private final static String REPARK = "/ReparkBin";
    private final static String GETREVERSEGEOCODE = "/getreversegeocode";
    private final static String SAVEBINJOB = "/savebinjob";
    private final static String SAVEBIN = "/savebin";
    private final static String ADDJOBPHOTO = "/UploadBinPhoto";
    private final static String LOGIN = "/login";

    public MyWebRequest(String apikey){
        this.APIKEY = apikey;
    }

    @Override
    protected String doInBackground(String... data) {
        // Perform your network operation.
        // Get JSON or XML string from the server.
        // Store in a local variable (say response) and return.

        //assign data[0] as the rfid
        String response = "";
        switch(data[0]){
            case "GETBIN":
                response = getBin(data[1]);
                break;
            case "GETBINS":
                response = getBins();
                break;
            case "UPDATEBINJOB":
                response = updateBinJob(data[1]);
                break;
            case "ADDJOBPHOTOS":
                response = addBinJobPhoto(data[1]);
                break;
            case "UPDATEJOBLOCATION":
                response = updateJobLocation(data[1]);
                break;
            case "REPARK":
                response = Repark(data[1]);
                break;
            case "GETREVERSEGEOCODE":
                response = getReverseGeoCode(data[1]);
                break;
            case "SAVEBINJOB":
                response = saveBinJob(data[1]);
                break;
            case "SAVEBIN":
                response = saveBin(data[1]);
                break;
            case "LOGIN":
                response = login(data[1]);
                break;
            default: break;
        }

        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.wtf("API RESPONSE", result);
        delegate.processFinish(result);
    }

    private String getCall(String geturl, HashMap<String, String> params){
        URL url;
        String response = "";
        try {
            Log.wtf("[GET] endpoint", geturl);
            url = new URL(geturl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(300000);
            conn.setConnectTimeout(300000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoInput(true);
            //conn.setDoOutput(true); //POST DOES NOT REQUIRE THIS

            if(!params.isEmpty()){
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(params));

                writer.flush();
                writer.close();
                os.close();
            }

            int responseCode=conn.getResponseCode();

            Log.wtf("[GET] RESPONSE CODE", String.valueOf(responseCode));
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else {
                response="";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    //post call using json string as params
    private String postCall(String posturl, String jsonstring){
        URL url;
        String response = "";
        try {
            Log.wtf("[POST] endpoint", posturl);
            url = new URL(posturl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(300000);
            conn.setConnectTimeout(300000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            if(!jsonstring.isEmpty()){
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(jsonstring);

                writer.flush();
                writer.close();
                os.close();
            }

            int responseCode=conn.getResponseCode();

            Log.wtf("[POST] RESPONSE CODE", String.valueOf(responseCode));
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else {
                response="";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    //post call using hashmap as params
    private String postCall(String posturl, HashMap<String, String> params){
        URL url;
        String response = "";
        try {
            Log.wtf("[POST] endpoint", posturl);
            url = new URL(posturl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            if(!params.isEmpty()){
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(maptoJson(params));

                writer.flush();
                writer.close();
                os.close();
            }

            int responseCode=conn.getResponseCode();

            Log.wtf("[POST] RESPONSE CODE", String.valueOf(responseCode));
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else {
                response="";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    protected String getBins() {
        //url
        String thisurl = uri + GETBINS;

        //params
        HashMap<String, String> postDataParams = new HashMap<String, String>();
        postDataParams.put("Key", this.APIKEY);

        return postCall(thisurl, postDataParams);
    }

    protected String getBin(String thisrfid) {
        //url
        String thisurl = uri + GETBIN;

        //params
        HashMap<String, String> postDataParams = new HashMap<String, String>();
        postDataParams.put("Key", this.APIKEY);
        postDataParams.put("Rfid", thisrfid);

        return postCall(thisurl, postDataParams);
    }

    protected String updateBinJob(String data){

        //url
        String thisurl = uri + UPDATEBINJOB;

        JsonParser parser = new JsonParser();

        //create payload
        String payload = "{}";

        //convert jsonstring back to json object
        JsonObject jsonjob = parser.parse(data).getAsJsonObject();
        JsonObject o = parser.parse(payload).getAsJsonObject();

        //add apikey to json object
        o.add("Key", new Gson().toJsonTree(this.APIKEY));
        o.add("Job", jsonjob);

        //convert back to json string
        String postDataParams = o.toString();
        Log.wtf("UPDATE BIN PARAMS", postDataParams);

        return postCall(thisurl, postDataParams);
    }

    protected String addBinJobPhoto(String data){

        //url
        String thisurl = uri + ADDJOBPHOTO;

        JsonParser parser = new JsonParser();

        //convert jsonstring back to json object
        JsonObject jsonjob = parser.parse(data).getAsJsonObject();

        //add apikey to json object
        jsonjob.add("Key", new Gson().toJsonTree(this.APIKEY));

        //convert back to json string
        String postDataParams = jsonjob.toString();
        Log.wtf("ADD JOB PHOTO PARAMS", postDataParams);

        return postCall(thisurl, postDataParams);
    }

    protected String updateJobLocation(String data) {
        //url
        String thisurl = uri + UPDATEJOBLOCATION;

        JsonParser parser = new JsonParser();

        //create payload
        String payload = "{}";

        //convert jsonstring back to json object
        JsonObject jsonjob = parser.parse(data).getAsJsonObject();
        JsonObject o = parser.parse(payload).getAsJsonObject();

        //add apikey to json object
        o.add("Key", new Gson().toJsonTree(this.APIKEY));
        o.add("JobLocation", jsonjob);

        //convert back to json string
        String postDataParams = o.toString();
        Log.wtf("UPDATE JOB LOCATION PARAMS", postDataParams);

        return postCall(thisurl, postDataParams);
    }

    protected String Repark(String data) {
        //url
        String thisurl = uri + REPARK;

        JsonParser parser = new JsonParser();

        //create payload
        String payload = "{}";

        //convert jsonstring back to json object
        JsonObject jsonjob = parser.parse(data).getAsJsonObject();
        JsonObject o = parser.parse(payload).getAsJsonObject();

        //add apikey to json object
        o.add("Key", new Gson().toJsonTree(this.APIKEY));
        o.add("Job", jsonjob);

        //convert back to json string
        String postDataParams = o.toString();
        Log.wtf("REPARK PARAMS", postDataParams);

        return postCall(thisurl, postDataParams);
    }

    protected String getReverseGeoCode(String data){
        //url
        String thisurl = uri + GETREVERSEGEOCODE;

        //convert jsonstring back to json object
        JsonParser parser = new JsonParser();
        JsonObject o = parser.parse(data).getAsJsonObject();

        //add apikey to json object
        o.add("Key", new Gson().toJsonTree(this.APIKEY));

        //convert back to json string
        String postDataParams = o.toString();
        Log.wtf("REVERSE GEOCODE PARAMS", postDataParams);

        return postCall(thisurl, postDataParams);
    }

    protected String saveBinJob(String data){

        //url
        String thisurl = uri + SAVEBINJOB;

        //create payload
        String payload = "{}";

        //convert jsonstring back to json object
        JsonParser parser = new JsonParser();
        JsonObject jsonjob = parser.parse(data).getAsJsonObject();
        JsonObject o = parser.parse(payload).getAsJsonObject();

        //add apikey to json object
        o.add("Key", new Gson().toJsonTree(this.APIKEY));
        o.add("Job", jsonjob);

        //convert back to json string
        String postDataParams = o.toString();
        Log.wtf("SAVEBINJOB PARAMS", postDataParams);

        return postCall(thisurl, postDataParams);
    }

    protected String saveBin(String data){

        //url
        String thisurl = uri + SAVEBIN;

        Log.wtf("SAVEBIN PARAMS", data);

        return postCall(thisurl, data);
    }

    protected String login(String data){
        //url
        String thisurl = uri + LOGIN;

        Log.wtf("LOGIN PARAMS", data);

        return postCall(thisurl, data);
    }

    //format params for normal form request
    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        Log.wtf("PARAMS", String.valueOf(result.toString()));
        return result.toString();
    }

    //format params for JSON requests
    private String maptoJson(HashMap<String, String> params){

        String jsonString = new Gson().toJson(params);
        Log.wtf("JSON PARAMS", jsonString);
        return jsonString;
    }
}

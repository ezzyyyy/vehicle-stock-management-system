package sg.drive.myuhfreader;

public interface AsyncResponse {
    void processFinish(String output);
}

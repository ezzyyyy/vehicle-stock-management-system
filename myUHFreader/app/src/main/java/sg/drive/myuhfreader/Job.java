package sg.drive.myuhfreader;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class Job {
    private boolean InTransit;
    private Integer JobID;
    private Integer BinID;
    private Integer JobType;
    private String Start;
    private String End;
    private String RFID;
    private JobInfo JobInfo;
    private Integer[] WasteTypes;
    private Map<String, Double> StartPosition;
    private Map<String, Double> EndPosition;
    private sg.drive.myuhfreader.Photos Photos;
    private String Geofences;

    public Job(){
        super();
    }

    public Job(HashMap<String, Object> jobdata){

        //convert ID to integer then string
        this.JobID = parseInt(convertToString(jobdata.get("JobID")));
        //Log.wtf("jobID", this.JobID);

        this.BinID = parseInt(convertToString(jobdata.get("BinID")));

        this.JobType = null;
        if(jobdata.get("JobType") != null){
            this.JobType = parseInt(convertToString(jobdata.get("JobType")));
        }

        this.Start = null;
        if(jobdata.get("Start") != null){
            this.Start = jobdata.get("Start").toString();
        }

        this.End = null;
        if(jobdata.get("End") != null){
            this.End = jobdata.get("End").toString();
        }

        this.InTransit = false;
        if(jobdata.get("InTransit") != null){
            this.InTransit = (boolean)jobdata.get("InTransit");
        }

        this.RFID = null;
        if(jobdata.get("RFID") != null){
            this.RFID = jobdata.get("RFID").toString();
        }

        this.Geofences = null;
        if(jobdata.get("Geofences") != null){
            this.Geofences = jobdata.get("Geofences").toString();
        }

        Gson gson = new Gson();

        //handling start positions
        this.StartPosition = null;
        if(jobdata.get("StartPosition") != null){
            String startpos = jobdata.get("StartPosition").toString();
            //Log.wtf("START POS", startpos);
            Map<String, Double> startpos_map = new HashMap<String, Double>();
            startpos_map = (Map<String, Double>) gson.fromJson(startpos, startpos_map.getClass());
            if((startpos_map.get("Latitude") != null) && (startpos_map.get("Longitude") != null)){
                this.StartPosition = new HashMap<String, Double>();
                this.StartPosition.put("Latitude", startpos_map.get("Latitude"));
                this.StartPosition.put("Longitude", startpos_map.get("Longitude"));
            }
        }

        //handling end positions
        this.EndPosition = null;
        if(jobdata.get("EndPosition") != null){
            String endpos = jobdata.get("EndPosition").toString();
            //Log.wtf("END POS", endpos);
            Map<String, Double> endpos_map = new HashMap<String, Double>();
            endpos_map = (Map<String, Double>) gson.fromJson(endpos, endpos_map.getClass());
            if((endpos_map.get("Latitude") != null) && (endpos_map.get("Longitude") != null)){
                this.EndPosition = new HashMap<String, Double>();
                this.EndPosition.put("Latitude", endpos_map.get("Latitude"));
                this.EndPosition.put("Longitude", endpos_map.get("Longitude"));
            }
        }

    }

    public Job(JsonObject jobdata, JsonObject ljobdata){
        //convert ID to integer then string
        this.JobID = parseInt(jobdata.get("JobID").toString());
        //Log.wtf("jobID", this.JobID);

        this.BinID = null;
        if((jobdata.get("BinID") != null) && (jobdata.get("BinID").toString() != "null")){
            this.BinID = parseInt(jobdata.get("BinID").toString());
        }

        this.JobType = null;
        if(jobdata.get("JobType").toString() != "null"){
            this.JobType = parseInt(jobdata.get("JobType").toString());
        }

        this.Start = null;
        if((jobdata.get("Start") != null) && (jobdata.get("Start").toString() != "null")){
            this.Start = jobdata.get("Start").toString();
        }

        this.End = null;
        if((jobdata.get("End") != null) && (jobdata.get("End").toString() != "null")){
            this.End = jobdata.get("End").toString();
        }

        this.InTransit = false;
        if(jobdata.get("InTransit").toString() != "null"){
            this.InTransit = jobdata.get("InTransit").getAsBoolean();
        }

        this.RFID = null;
        if((jobdata.get("RFID")) != null && (jobdata.get("RFID").toString() != "null")){
            this.RFID = jobdata.get("RFID").toString();
        }

        this.Geofences = null;
        if((jobdata.get("Geofences")) != null && (jobdata.get("Geofences").toString() != "null")){
            this.Geofences = jobdata.get("Geofences").toString();
        }

        this.JobInfo = new JobInfo(ljobdata);

        //handling start positions
        this.StartPosition = null;
        boolean sPos = !("null".equalsIgnoreCase(jobdata.get("StartPosition").toString()));
        if(sPos){
            JsonObject startpos = jobdata.getAsJsonObject("StartPosition");
            if((startpos.get("Latitude") != null) && (startpos.get("Longitude") != null)){
                this.StartPosition = new HashMap<>();
                this.StartPosition.put("Latitude", Double.parseDouble(startpos.get("Latitude").toString()));
                this.StartPosition.put("Longitude", Double.parseDouble(startpos.get("Longitude").toString()));
            }
        }

        //handling end positions
        this.EndPosition = null;
        boolean ePos = !("null".equalsIgnoreCase(jobdata.get("EndPosition").toString()));
        if(ePos){
            JsonObject endpos = jobdata.getAsJsonObject("EndPosition");
            if((endpos.get("Latitude") != null) && (endpos.get("Longitude") != null)){
                this.EndPosition = new HashMap<>();
                this.EndPosition.put("Latitude", Double.parseDouble(endpos.get("Latitude").toString()));
                this.EndPosition.put("Longitude", Double.parseDouble(endpos.get("Longitude").toString()));
            }
        }

    }

    public Integer getJobID(){
        return this.JobID;
    }

    public void setJobID(Integer jid) {
        this.JobID = jid;
    }

    public Integer getBinID(){
        return this.BinID;
    }

    public void setBinID(Integer bid) {
        this.BinID = bid;
    }

    public Integer getJobType(){
        return this.JobType;
    }

    public void setJobType(Integer jt){
        this.JobType = jt;
    }

    public Map<String, Double> getStartPosition(){ ;
        return this.StartPosition;
    }

    public void setStartPosition(Map<String, Double> sp){ ;
        this.StartPosition = sp;
    }

    public Map<String, Double> getEndPosition(){
        return this.EndPosition;
    }

    public void setEndPosition(Map<String, Double> ep){ ;
        this.EndPosition = ep;
    }

    public boolean getInTransit(){
        return this.InTransit;
    }

    public void setInTransit(boolean transit){
        this.InTransit = transit;
    }

    public String getStart(){
        return this.Start;
    }

    public String getEnd(){
        return this.End;
    }

    public void setStart(String start){
        this.Start = start;
    }

    public void setEnd(String end) {
        this.End = end;
    }

    public void setRFID(String rfid){
        this.RFID = rfid;
    }

    public void setJobInfo(JobInfo jobinfo) { this.JobInfo = jobinfo; }

    public JobInfo getJobInfo() {
        return JobInfo;
    }

    public void setPhotos(sg.drive.myuhfreader.Photos photos) {
        Photos = photos;
    }

    public void setWasteTypes(Integer[] wasteTypes) {
        WasteTypes = wasteTypes;
    }

    public Integer[] getWasteTypes() {
        return WasteTypes;
    }

    public sg.drive.myuhfreader.Photos getPhotos() {
        return Photos;
    }

    public String getRFID(){
        return this.RFID;
    }

    public String getGeofences(){
        return this.Geofences;
    }

    public void setGeofences(String geofences){
        this.Geofences = geofences;
    }

    private String convertToString(Object data){
        if (data.getClass().equals(Double.class)){
            Integer res = ((Double)data).intValue();
            return res.toString();
        }else if (data.getClass().equals(String.class)){
            return data.toString();
        }else{
            return null;
        }
    }
}

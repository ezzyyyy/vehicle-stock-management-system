package sg.drive.myuhfreader;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Line;

import org.checkerframework.checker.linear.qual.Linear;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class ReportIncidentDialog extends DialogFragment {
    private static final String TAG = "ReportIncidentDialog";
    private LinearLayout incidentButton1, incidentButton2, incidentButton3, incidentButton4, incidentButton5, incidentButton6;
    private TextView incDesc1, incDesc2, incDesc3, incDesc4, incDesc5, incDesc6;
    private Button reportIncidentSubmit;
    private String imageFilePath;

    private EditText incidentDesc;
    private String otherIncidentDesc;

    private LinearLayout currentSelectedButton;
    private ImageView incidentPhotoBtn;
    static final int REQUEST_IMAGE_CAPTURE = 124;

    private HashMap<Integer, Integer> allIncidents;
    private HashMap<Integer, String> savedIncidents;
    private HashMap<LinearLayout,Integer> selectedIncidents;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.report_incident_fragment, container, false);
        selectedIncidents = new HashMap<>();
        allIncidents = new HashMap<>();
        otherIncidentDesc = "";

        savedIncidents = new HashMap<>();
        reportIncidentSubmit = view.findViewById(R.id.report_incident_submit);
        incidentDesc = view.findViewById(R.id.incident_desc);

        initAllIncidents(view);

        incidentButton1.setOnClickListener(this::changeBackground);
        incidentButton2.setOnClickListener(this::changeBackground);
        incidentButton3.setOnClickListener(this::changeBackground);
        incidentButton4.setOnClickListener(this::changeBackground);
        incidentButton5.setOnClickListener(this::changeBackground);
        incidentButton6.setOnClickListener(this::changeBackground);

        //initializing the camera button
        incidentPhotoBtn = view.findViewById(R.id.incident_photo_btn);

        incidentPhotoBtn.setOnClickListener(v -> {
            launchCamera();
        });

        reportIncidentSubmit.setOnClickListener(v->{ //upon submit, send the description of the waste types to bin activity
            if(incidentDesc.getText()!=null){
                otherIncidentDesc += incidentDesc.getText().toString();
            }

            //api needs to check if otherIncidentDesc is empty/null

            ((BinActivity)getActivity()).saveIncidentDescInstance(savedIncidents,otherIncidentDesc); //set the waste type in bin activity as the content in  wasteTypeDesc (edittext)
            dismiss();
        });


        return view;
    }

    public void changeBackground(View v){
//        if(currentSelectedButton != null)
//            currentSelectedButton.setBackgroundColor(0x00000000); //set the original button pressed as transparent background

        currentSelectedButton = (LinearLayout) v; //set the selected button to the new button you press

        Integer status = selectedIncidents.get(currentSelectedButton);

        if(status == null){ //if the button has not been selected before
            //status: 1 is selected and 2 unselected
            selectedIncidents.put(currentSelectedButton, new Integer(1));
            currentSelectedButton.setBackgroundColor(getResources().getColor(R.color.transparent_red, ((BinActivity)getActivity()).getTheme()));
            saveIncident(currentSelectedButton);
        } else { //button has been selected before, check if the previous status is a select or disselect

            if(status.intValue() == 1){ //if the button has been previously selected
                currentSelectedButton.setBackgroundColor(0x00000000);
                selectedIncidents.put(currentSelectedButton, new Integer(2));
                removeSavedIncident(currentSelectedButton);

            } else if(status.intValue() == 2){ //if the button has been deselected previously
                currentSelectedButton.setBackgroundColor(getResources().getColor(R.color.transparent_red, ((BinActivity)getActivity()).getTheme()));
                selectedIncidents.put(currentSelectedButton, new Integer(1));
                saveIncident(currentSelectedButton);
            }

        }
    }

    private boolean hasCamera(){
        return getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    public void launchCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Take a picture and pass results along to onActivityResult

        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            //create file to store the image
            File photoFile = null;
            try{
                photoFile = createImageFile();
            } catch (IOException exception){
                Toast.makeText(getActivity(), "Unable to create file", Toast.LENGTH_LONG).show();
            }

            if(photoFile != null){ //only when file can be created
                Uri photoURI = FileProvider.getUriForFile(getActivity(),"sg.drive.myuhfreader.provider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
                //Take a picture and pass results along to onActivityResult
                startActivityForResult(intent,REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timestamp = new SimpleDateFormat("dd.MM.yyyy_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timestamp;
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,".jpg",storageDir);

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode==RESULT_OK) {
//            //get the photo
//            Bundle extras = data.getExtras();
//            Bitmap photo = (Bitmap) extras.get("data");

            int viewW = incidentPhotoBtn.getWidth();
            int viewH = incidentPhotoBtn.getHeight();

            //get bitmap's dimensions
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imageFilePath,bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            //how much to scale down the image
            int scaleFactor = Math.min(photoW/viewW, photoH/viewH);

            //Decode image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap btp = BitmapFactory.decodeFile(imageFilePath,bmOptions);
            incidentPhotoBtn.setImageBitmap(btp);
            ((BinActivity)getActivity()).saveImageFilePath(imageFilePath);

//            incidentPhotoBtn.setImageBitmap(photo);
        }
    }

    public void saveIncident(LinearLayout selectedIncident){

        switch(selectedIncident.getId())
        {
            case R.id.inc1:
                savedIncidents.put(allIncidents.get(R.id.inc1), incDesc1.getText().toString());
                break;

            case R.id.inc2:
                savedIncidents.put(allIncidents.get(R.id.inc2), incDesc2.getText().toString());
                break;

            case R.id.inc3:
                savedIncidents.put(allIncidents.get(R.id.inc3), incDesc3.getText().toString());
                break;

            case R.id.inc4:
                savedIncidents.put(allIncidents.get(R.id.inc4), incDesc4.getText().toString());
                break;

            case R.id.inc5:
                savedIncidents.put(allIncidents.get(R.id.inc5), incDesc5.getText().toString());
                break;

            case R.id.inc6:
                savedIncidents.put(allIncidents.get(R.id.inc6), incDesc6.getText().toString());
                break;
        }

    }

    public void removeSavedIncident(LinearLayout selectedWaste){ //add waste type to desc upon selection

        switch(selectedWaste.getId())
        {
            case R.id.inc1:
                savedIncidents.remove(allIncidents.get(R.id.inc1));
                break;

            case R.id.inc2:
                savedIncidents.remove(allIncidents.get(R.id.inc2));
                break;

            case R.id.inc3:
                savedIncidents.remove(allIncidents.get(R.id.inc3));
                break;

            case R.id.inc4:
                savedIncidents.remove(allIncidents.get(R.id.inc4));
                break;

            case R.id.inc5:
                savedIncidents.remove(allIncidents.get(R.id.inc5));
                break;

            case R.id.inc6:
                savedIncidents.remove(allIncidents.get(R.id.inc6));
                break;
        }

    }

    private void initAllIncidents(View v){
        incidentButton1 = v.findViewById(R.id.inc1);
        allIncidents.put(R.id.inc1,1);
        incidentButton2 = v.findViewById(R.id.inc2);
        allIncidents.put(R.id.inc2,2);
        incidentButton3 = v.findViewById(R.id.inc3);
        allIncidents.put(R.id.inc3,3);
        incidentButton4 = v.findViewById(R.id.inc4);
        allIncidents.put(R.id.inc4,4);
        incidentButton5 = v.findViewById(R.id.inc5);
        allIncidents.put(R.id.inc5,5);
        incidentButton6 = v.findViewById(R.id.inc6);
        allIncidents.put(R.id.inc6,6);

        incDesc1 = v.findViewById(R.id.inc_desc1);
        incDesc2 = v.findViewById(R.id.inc_desc2);
        incDesc3 = v.findViewById(R.id.inc_desc3);
        incDesc4 = v.findViewById(R.id.inc_desc4);
        incDesc5 = v.findViewById(R.id.inc_desc5);
        incDesc6 = v.findViewById(R.id.inc_desc6);
    }

    @Override
    public void dismiss(){
        super.dismiss();
    }
}

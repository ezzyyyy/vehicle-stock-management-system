package sg.drive.myuhfreader;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class IncidentPopup extends DialogFragment {

    private static final String TAG = "IncidentPopup";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.report_incident_fragment,container,false);

        return view;
    }


}

package sg.drive.myuhfreader;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.rscja.deviceapi.RFIDWithUHF;
import com.rscja.utility.StringUtility;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import sg.drive.myuhfreader.parser.NdefMessageParser;
import sg.drive.myuhfreader.record.ParsedNdefRecord;

import static java.util.Locale.getDefault;

public class ScanActivity extends AppCompatActivity implements AsyncResponse {

    TextInputEditText input_manualrfid;
    Button btn_manualrfid;
    private static final int PERMISSION_REQUEST_CODE_FINE_LOCATION = 1;
    private static final int PERMISSION_REQUEST_CODE_COARSE_LOCATION = 2;
    //NFC Variables
    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;
    private TextView text;
    private String savedNFCdata;
    private String BinResult;
    private Handler dialogHandler;
    Gson gson;

    String endpoint = "";
    String modeselected = "";
    String loginkey;
    String tagName;
    String nfcID;

    // UHF variables
    public boolean loopFlag = false;
    public ProgressDialog myDialog;
    public RFIDWithUHF mReader;
    public Handler handler;
    private String LastReadUHF;
    TagThread mThread;
    ImageView logoutBtn;
    ImageView createTagBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        createTagBtn = findViewById((R.id.create_rfid));
        createTagBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createTag();
                    }
                }
        );

        logoutBtn = findViewById(R.id.logout_btn);
        logoutBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getIntent().removeExtra("loginkey");

                        //save creds to SP
                        SharedPreferences sp = getSharedPreferences("Login", MODE_PRIVATE);
                        SharedPreferences.Editor Ed = sp.edit();
                        Ed.putString("user", null);
                        Ed.putString("pass", null);
                        Ed.commit();

                        movetoLogin();
                    }
                }
        );

        myDialog = new ProgressDialog(this);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        gson = new Gson();

        // init handlers
        dialogHandler = new Handler();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String result = msg.obj.toString();
                logUHFValue(result);
                playSound(1);
            }
        };

        // INIT uhf
        showDialog(getString(R.string.init_uhf));
        initSound();
        updateDialog(getString(R.string.check_uhf), 1000);
        //initUHF();

        if(checkLogin(savedInstanceState)){
            initLayout();
        }else{
            movetoLogin();
        }
    }

    public void createTag() {
        nfcID = input_manualrfid.getText().toString();
        Integer binID = 0;

        if (!input_manualrfid.getText().toString().isEmpty()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Set tag name for (" + nfcID + ")");
            alert.setMessage("A name for this NFC tag is required. Please set its name on the input below.");

            // Set an EditText view to get user input
            final EditText input = new EditText(this);
            alert.setView(input);

            alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    tagName = input.getText().toString();
                    Log.d("", "Pin Value : " + tagName);
                    if (tagName.isEmpty()) {
                        showToast();
                    } else {
                        //create payload
                        String payload = "{}";

                        //convert jsonstring back to json object
                        JsonParser parser = new JsonParser();
                        JsonObject o = parser.parse(payload).getAsJsonObject();

                        //add apikey to json object
                        o.add("Key", new Gson().toJsonTree(loginkey));
                        o.add("RFID", new Gson().toJsonTree(nfcID));
                        o.add("Name", new Gson().toJsonTree(tagName));
                        o.add("BinID", new Gson().toJsonTree(binID));

                        String postDataParams = o.toString();
                        endpoint = "SAVEBIN";
                        String[] data = {endpoint, postDataParams};
                        httpRequest(data);

                        return;
                    }
                }
            });

            alert.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            return;
                        }
                    });

            alert.show();
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Please scan your NFC tag or input the tag ID manually.");
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    return;
                }
            });

            alert.show();
        }
    }

    public void showToast() {
        Toast toast = Toast.makeText(this, "Name is required to save this tag.", Toast.LENGTH_LONG);
        ViewGroup group = (ViewGroup) toast.getView();
        TextView messageTextView = (TextView) group.getChildAt(0);
        messageTextView.setTextSize(25);
        toast.setGravity(Gravity.BOTTOM, 0, 800);
        toast.show();
    }

    public void getLocations()
    {
        Timer timer = new Timer ();
        TimerTask locationTask = new TimerTask () {
            @Override
            public void run () {

            }
        };

        timer.schedule (locationTask, 0l, 3000);
    }

    public boolean checkLogin(Bundle savedInstanceState){
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                loginkey = null;
            } else {
                loginkey = extras.getString("loginkey");
            }
        } else {
            loginkey = (String) savedInstanceState.getSerializable("loginkey");
        }

        if(loginkey == null){
            return false;
        }else{
            return true;
        }
    }

    public void movetoLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void initLayout(){
        input_manualrfid = findViewById((R.id.input_rfid));
        btn_manualrfid = findViewById((R.id.btn_enter));

        // input_manualrfid.setText("e0 04 01 00 a3 ca 44 f6");
    }

    public void submitRFID(View view){
        String rfid = input_manualrfid.getText().toString();
        if (rfid.isEmpty()) {
            Toast.makeText(this, R.string.id_required, Toast.LENGTH_LONG).show();
        } else {
            showDialog("Checking RFID info...");
            getBinData(rfid);
        }
    }

    public void initScanner()
    {
        if (nfcAdapter == null) {
            Toast.makeText(this, R.string.no_nfc_prompt, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (nfcAdapter != null) {
            if (!nfcAdapter.isEnabled())
                showWirelessSettings();
        }

        initScanner();

        hideDialog(3000);
    }

    private void showWirelessSettings() {
        Toast.makeText(this, R.string.enable_nfc_prompt, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    private void resolveIntent(Intent intent) {
        String action = intent.getAction();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs;

            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];

                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }

            } else {
                byte[] empty = new byte[0];
                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                byte[] payload = dumpTagData(tag).getBytes();
                NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, id, payload);
                NdefMessage msg = new NdefMessage(new NdefRecord[] {record});
                msgs = new NdefMessage[] {msg};
            }

            displayMsgs(msgs);
        }
    }

    private void displayMsgs(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0)
            return;

        StringBuilder builder = new StringBuilder();
        List<ParsedNdefRecord> records = NdefMessageParser.parse(msgs[0]);
        final int size = records.size();

        for (int i = 0; i < size; i++) {
            ParsedNdefRecord record = records.get(i);
            String str = record.str();
            builder.append(str).append("\n");
        }

        //input_manualrfid.setText(builder.toString());
        input_manualrfid.setText(savedNFCdata);
    }

    private String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    private String toReversedHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; ++i) {
            if (i > 0) {
                sb.append(" ");
            }
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
        }
        return sb.toString();
    }

    private long toDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private long toReversedDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = bytes.length - 1; i >= 0; --i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private String dumpTagData(Tag tag) {
        StringBuilder sb = new StringBuilder();
        byte[] id = tag.getId();
        sb.append("ID (hex): ").append(toHex(id)).append('\n');
        //sb.append("ID (reversed hex): ").append(toReversedHex(id)).append('\n');
        //sb.append("ID (dec): ").append(toDec(id)).append('\n');
        //sb.append("ID (reversed dec): ").append(toReversedDec(id)).append('\n');

        savedNFCdata = (String) toHex(id);
        Log.wtf("NFC TEXT", savedNFCdata);

        return sb.toString();
        /*
        String prefix = "android.nfc.tech.";
        sb.append("Technologies: ");
        for (String tech : tag.getTechList()) {
            sb.append(tech.substring(prefix.length()));
            sb.append(", ");
        }

        sb.delete(sb.length() - 2, sb.length());

        for (String tech : tag.getTechList()) {
            if (tech.equals(MifareClassic.class.getName())) {
                sb.append('\n');
                String type = "Unknown";

                try {
                    MifareClassic mifareTag = MifareClassic.get(tag);

                    switch (mifareTag.getType()) {
                        case MifareClassic.TYPE_CLASSIC:
                            type = "Classic";
                            break;
                        case MifareClassic.TYPE_PLUS:
                            type = "Plus";
                            break;
                        case MifareClassic.TYPE_PRO:
                            type = "Pro";
                            break;
                    }
                    sb.append("Mifare Classic type: ");
                    sb.append(type);
                    sb.append('\n');

                    sb.append("Mifare size: ");
                    sb.append(mifareTag.getSize() + " bytes");
                    sb.append('\n');

                    sb.append("Mifare sectors: ");
                    sb.append(mifareTag.getSectorCount());
                    sb.append('\n');

                    sb.append("Mifare blocks: ");
                    sb.append(mifareTag.getBlockCount());
                } catch (Exception e) {
                    sb.append("Mifare classic error: " + e.getMessage());
                }
            }

            if (tech.equals(MifareUltralight.class.getName())) {
                sb.append('\n');
                MifareUltralight mifareUlTag = MifareUltralight.get(tag);
                String type = "Unknown";
                switch (mifareUlTag.getType()) {
                    case MifareUltralight.TYPE_ULTRALIGHT:
                        type = "Ultralight";
                        break;
                    case MifareUltralight.TYPE_ULTRALIGHT_C:
                        type = "Ultralight C";
                        break;
                }
                sb.append("Mifare Ultralight type: ");
                sb.append(type);
            }
        }

        return sb.toString();
        */
    }

    public void getBinData(String rfid){
        endpoint = "GETBIN";
        String[] bin_data = {endpoint, rfid};
        httpRequest(bin_data);
    }

    public void httpRequest(String[] request) {
        //execute the async task
        MyWebRequest asyncTask = new MyWebRequest(loginkey);
        asyncTask.delegate = this;

        asyncTask.execute(request);
    }

    @Override
    public void processFinish(String output){
        processResult(output);
    }

    public void processResult(String result){
        try {
            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(result).getAsJsonObject();
            if (endpoint == "GETBIN") {
                JsonObject BinData = o.getAsJsonObject("GetBinResult");
                if (BinData != null) {
                    JsonElement binID = BinData.get("BinID");
                    JsonElement binStatus = BinData.get("Status");
                    if (binID != null && binStatus.toString().equals("1")) {
                        BinResult = result;
                        hideDialog(300);
                        moveToBin();
                    } else if(binID != null && binStatus.toString().equals("0")) {
                        Toast toast = Toast.makeText(this, R.string.bin_under_maintenance_reminder, Toast.LENGTH_LONG);
                        ViewGroup group = (ViewGroup) toast.getView();
                        TextView messageTextView = (TextView) group.getChildAt(0);
                        messageTextView.setTextSize(25);
                        toast.setGravity(Gravity.BOTTOM, 0, 800);
                        toast.show();
                    } else {
                        Toast.makeText(this, R.string.no_bin_found, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(this, R.string.oops_error, Toast.LENGTH_LONG).show();
                }
            } else if (endpoint == "SAVEBIN") {
                JsonObject BinData = o.getAsJsonObject("SaveBinResult");
                if (BinData != null) {
                    JsonElement msg = BinData.get("Message");
                    Log.wtf("data", msg.toString());

                    if (msg.getAsString().contains("OK")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.setTitle("Success!");
                        alert.setMessage("The tag has been created. \n Name: " + tagName + "\n RFID: " + nfcID);

                        alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                return;
                            }
                        });

                        alert.show();
                    } else if(msg.getAsString().contains("RECORD_EXISTING")) {
                        Toast toast = Toast.makeText(this, "This tag already exists.", Toast.LENGTH_LONG);
                        ViewGroup group = (ViewGroup) toast.getView();
                        TextView messageTextView = (TextView) group.getChildAt(0);
                        messageTextView.setTextSize(25);
                        toast.setGravity(Gravity.BOTTOM, 0, 800);
                        toast.show();
                    } else {
                        Toast.makeText(this, "Error! Please try again.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(this, R.string.oops_error, Toast.LENGTH_LONG).show();
                }
            }
        }
        catch (Exception e)
        {
            Log.wtf("Error", e.getMessage());
        }
    }

    private void moveToBin()
    {
        Intent intent = new Intent(this, BinActivity.class);
        intent.putExtra("loginkey", loginkey);
        intent.putExtra("BinResult", BinResult);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (mReader != null)
            mReader.free();

        movetoLogin();
    }

    @Override
    protected void onDestroy() {

        if (mReader != null) {
            mReader.free();
        }
        super.onDestroy();
    }

    private void showDialog(String msg)
    {
        updateDialog(msg, 0);
        myDialog.show();
    }

    private void updateDialog(String msg, int delayTime)
    {
        dialogHandler.postDelayed(() -> myDialog.setMessage(msg), delayTime);
    }

    private void hideDialog(int delayTime)
    {
        dialogHandler.postDelayed(() -> myDialog.hide(), delayTime);
    }

    // UHF Async Tasks
    public void initUHF() {

        try {
            mReader = RFIDWithUHF.getInstance();
        } catch (Exception ex) {

            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT);

            return;
        }

        if (mReader != null) {
            new InitTask().execute();
        }
    }

    public void initReader() {
        if(loopFlag == false)
            loopFlag = true;
        else
            loopFlag = false;
        boolean loop = false;

        mReader.init();

        if (!loop && mReader.startInventoryTag((byte) 0, (byte) 0)) {
            try {
//                if(mThread == null || !mThread.isAlive())
//                    mThread = new TagThread();
//
//                if(!mThread.isAlive()) {
//                    mThread.start();
//                }
                String strTid;
                String[] res = null;
                loop = true;
                while (loop) {
                    res = mReader.readTagFromBuffer();
                    if (res != null && LastReadUHF != res[1]) {
                        strTid = res[1];
                        LastReadUHF = strTid;
                        Message msg = handler.obtainMessage();
                        msg.obj = strTid;
                        handler.sendMessage(msg);
                        loop = false;
                    }
                }

                mReader.free();
            } catch(Exception e)
            {
                Log.wtf("???", e.getMessage());
            }
        }
    }

    public class InitTask extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                return mReader.init();
            }
            catch (Exception e)
            {
                Log.wtf("mReader", e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if (!result) {
                updateDialog(getString(R.string.no_uhf_found), 1000);
                updateDialog(getString(R.string.nfc_loaded), 1000);
            }else
                updateDialog(getString(R.string.uhf_nfc_loaded), 1000);

            hideDialog(3000);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

        }

    }

//    public void logUHFValue(String uhfid)
//    {
//        uhfid = uhfid.substring(4,uhfid.length());
//        StringBuilder output = new StringBuilder();
//        for (int i = 0; i < uhfid.length(); i+=2) {
//            String str = uhfid.substring(i, i+2);
//            output.append((char)Integer.parseInt(str, 16));
//        }
//        input_manualrfid.setText(output);
//
//    }


    public void logUHFValue(String uhfid)
    {
        uhfid = uhfid.substring(uhfid.length() - 8,uhfid.length());

        input_manualrfid.setText(uhfid);

    }

    class TagThread extends Thread {
        public void run() {
            try {
                String strTid;
                String[] res = null;
                while (true) {
                    Thread.sleep(60);
                    res = mReader.readTagFromBuffer();
                    if (res != null && LastReadUHF != res[1]) {
                        strTid = res[1];
                        LastReadUHF = strTid;
                        Message msg = handler.obtainMessage();
                        msg.obj = strTid;
                        handler.sendMessage(msg);

                        throw new InterruptedException();
                    }
                }
            } catch (InterruptedException e){
                //throw new RuntimeException("Interrupted", e);
            }
        }
    }

    HashMap<Integer, Integer> soundMap = new HashMap<>();
    private SoundPool soundPool;
    private float volumnRatio;
    private AudioManager am;
    private void initSound(){
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 5);
        soundMap.put(1, soundPool.load(this, R.raw.barcodebeep, 1));
        soundMap.put(2, soundPool.load(this, R.raw.serror, 1));
        am = (AudioManager) this.getSystemService(AUDIO_SERVICE);
    }
    public void playSound(int id) {

        float audioMaxVolumn = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC); // 返回当前AudioManager对象的最大音量值
        float audioCurrentVolumn = am.getStreamVolume(AudioManager.STREAM_MUSIC);// 返回当前AudioManager对象的音量值
        volumnRatio = audioCurrentVolumn / audioMaxVolumn;
        try {
            soundPool.play(soundMap.get(id), volumnRatio, volumnRatio, 1,0,1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

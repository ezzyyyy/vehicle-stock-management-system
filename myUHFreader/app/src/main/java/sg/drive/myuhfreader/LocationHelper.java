package sg.drive.myuhfreader;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;

public class LocationHelper {
    private LocationCallback mLocationCallback;

    public interface LocationHelperListener
    {
        void UpdateLocation(LocationInfo location);
    }

    private FusedLocationProviderClient mFusedLocationClient;
    private Activity activity;
    private LocationHelperListener lListener;
    private Geocoder geocoder;
    private LocationRequest mLocationRequest;
    @SuppressLint("RestrictedApi")
    LocationHelper(Activity _activity)
    {
        this.mFusedLocationClient = LocationServices.getFusedLocationProviderClient(_activity);
        this.activity = _activity;
        lListener = (LocationHelper.LocationHelperListener) _activity;

        geocoder = new Geocoder(_activity, Locale.getDefault());

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    void getLocation()
    {
        LocationInfo loc = new LocationInfo();
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);

            mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(activity, location -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        loc.setAddress(String.valueOf(location.getAccuracy()));
                        loc.setLat(location.getLatitude());
                        loc.setLng(location.getLongitude());

                        List<Address> addresses;
                        try {
                            addresses = geocoder.getFromLocation(loc.getLat(), loc.getLng(), 1);

                            for(Address add :addresses)
                            {
                                loc.setAddress(add.getAddressLine(0) + ", " + add.getLocality());
                            }
                        } catch (IOException e) {
                            Log.wtf("REVERSE GEOCODE", e.getMessage());
                        }

                        lListener.UpdateLocation(loc);
                    }
                }).addOnFailureListener(activity, fail -> {
                    Log.wtf("Err", fail.getMessage());
            });

        }
        catch(Exception e)
        {
            Log.wtf("Err", e.getMessage());
        }
    }
}

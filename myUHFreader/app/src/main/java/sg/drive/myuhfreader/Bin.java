package sg.drive.myuhfreader;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Bin {
    private Integer BinID; //the bin's ID
    private Job Job; //what's its current job
    private String RFID; //the RFID tag ID
    private String Name; //the RFID tag ID
    private String Location; //the bin current location
    private String Geofences; //the bin current location
    private Integer Status; //status of the bin (deployed, under maintenance, ready for deployment)
    private Integer LastJobID;
    private Integer LastJobType;
    private JobInfo LastJobInfo;

    public Bin(){
        super(); //Default constructor: inherits the constructor of direct parent class, Object
    }

    public Bin(JsonObject bindata) { //constructor using bindata
        this.BinID = Integer.parseInt(bindata.get("BinID").toString()); //obtain binID in obj format then convert to string then integer
        this.Status = Integer.parseInt(bindata.get("Status").toString()); //obtain status of the bin (deployed, under maintenance, ready for deployment)
        this.RFID = bindata.get("RFID").toString(); //obtain RFID ID of the bin
        this.Name = bindata.get("Name").toString(); //obtain Name of the bin
        this.Location = bindata.get("Location").toString();
        this.Geofences = firstThree(bindata.get("Geofences").toString());


        if(bindata.getAsJsonObject("LastJob") != null) {
            this.LastJobInfo = new JobInfo(bindata.getAsJsonObject("LastJob"));
            this.LastJobType = LastJobInfo.getJobType();
            this.LastJobID = LastJobInfo.getJobID();
        }

        //handling jobs
        boolean hasJob = !("null".equalsIgnoreCase(bindata.get("Job").toString()));
        if(hasJob) { //if there is a Job assigned to the bin
            JsonObject jobdata = bindata.getAsJsonObject("Job"); //obtain the Job type of the Job assigned to the bin in String
            JsonObject ljobdata = bindata.getAsJsonObject("LastJob");
            Log.wtf("JOB DATA", jobdata.toString());

            this.Job = new Job(jobdata, ljobdata);

            String jid = String.valueOf(this.Job.getJobID());
            Log.wtf("JOB CREATED", jid);
        }
    }

    public Integer getBinID(){
        return this.BinID;
    }

    public String getRFID(){
        return this.RFID;
    }

    public String getName() {
        return Name;
    }

    public Job getJob(){
        return this.Job;
    }

    public void setStatus(Integer status){
        this.Status = status;
    }

    public Integer getStatus(){
        return this.Status;
    }

    private String convertToString(Object data){
        if(data == null){ //if there is no mapping for the bindata hashmap
            return null;
        }else{
            if (data.getClass().equals(Double.class)){ //if the value mapped to the key is a Double
                Integer res = ((Double)data).intValue();
                return res.toString();
            }else if (data.getClass().equals(String.class)){ //if the value mapped to the key is a String
                return data.toString();
            }else{
                return null;
            }
        }
    }

    public String getLocation() {
        return Location;
    }

    public Integer getLastJobType() { return this.LastJobType; }

    public void setLastJobInfo(JobInfo lastJobInfo) {
        LastJobInfo = lastJobInfo;
    }

    public JobInfo getLastJobInfo() {
        return LastJobInfo;
    }

    public void setLastJobID(Integer lastJobID) {
        LastJobID = lastJobID;
    }

    public Integer getLastJobID() {
        return LastJobID;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getGeofences() {
        return Geofences;
    }

    public void setGeofences(String geofences) {
        Geofences = geofences;
    }

    public String firstThree(String str) {
        return str.length() < 3 ? str : str.substring(1, 4);
    }
}


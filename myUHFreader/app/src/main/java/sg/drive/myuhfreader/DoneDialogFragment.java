package sg.drive.myuhfreader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;

public class DoneDialogFragment extends DialogFragment {
    public interface DoneDialogListener {
        void doneOk(DialogFragment dialog);
        void doneNOK(DialogFragment dialog);
    }

    DoneDialogFragment.DoneDialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if (context instanceof Activity){
            activity = (Activity) context;

            // Verify that the host activity implements the callback interface
            try {
                // Instantiate the NoticeDialogListener so we can send events to the host
                mListener = (DoneDialogFragment.DoneDialogListener) activity;
            } catch (ClassCastException e) {
                // The activity doesn't implement the interface, throw exception
                throw new ClassCastException(activity.toString() + " must implement DoneDialogListener");
            }
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String msg = getResources().getString(R.string.confirm_done);
        builder.setMessage(Html.fromHtml(msg))
            .setPositiveButton(getString(R.string.confirm_yes), (dialog, id) -> mListener.doneOk(DoneDialogFragment.this))
            .setNegativeButton(getString(R.string.confirm_no), (dialog, id) -> mListener.doneNOK(DoneDialogFragment.this));

        return builder.create();
    }
}

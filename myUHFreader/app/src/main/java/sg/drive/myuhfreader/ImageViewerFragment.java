package sg.drive.myuhfreader;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ImageViewerFragment extends DialogFragment{
    private static final String TAG = "ImageViewerFragment";
    public ImageView image;
    public Bitmap bitmapImage;
    private ImageView imageFragmentClose;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_view_fragment, container, false);

        image = view.findViewById(R.id.image_view_zoom);
        image.setImageBitmap(bitmapImage);
//        image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageFragmentClose = view.findViewById(R.id.image_fragment_close);

        imageFragmentClose.setOnClickListener(v -> {
            super.dismiss();
        });


        return view;
    }


    @Override
    public void dismiss(){
        super.dismiss();
    }
}



package sg.drive.myuhfreader;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationResult;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.victor.loading.rotate.RotateLoading;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import sg.drive.myuhfreader.Photos;

public class BinActivity extends FragmentActivity
        implements AsyncResponse,
        OnMapReadyCallback,
        com.google.android.gms.location.LocationListener,
        DeployDialogFragment.DeployDialogListener,
        CollectDialogFragment.CollectDialogListener,
        ReparkDialogFragment.ReparkDialogListener,
        DoneDialogFragment.DoneDialogListener,
        LocationHelper.LocationHelperListener
{

    //GPS variables
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationListener mListener;
    private static final int PERMISSION_REQUEST_CODE_FINE_LOCATION = 1;
    private static final int PERMISSION_REQUEST_CODE_COARSE_LOCATION = 2;

    //GPS Google Play
    private FusedLocationProviderClient mFusedClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;

    String endpoint = "";
    String modeselected = "";
    TextView textinput_location;
    String BinResult;
    Bin thisbin;
    Job currentJob;
    String rfid;
    Button btn_deploy;
    Button btn_collect;
    Button btn_done;
    Gson gson;
    String loginkey;

    String currAddress;
    Double currLat;
    Double currLng;
    Location lastLocation;
    Location LastSentLocation;
    Boolean SendLocation = false;
    Boolean HasJob = false;
    Calendar next = null;

    static final int CONFIRM_JOB = 1;
    static final int CONFIRM_BACK_PRESSED = 2;

    static final int JOBTYPE_STORAGE = 0;
    static final int JOBTYPE_DELIVER = 1;
    static final int JOBTYPE_REGISTER = 3;

    public ProgressDialog myDialog;
    private Handler dialogHandler;

    //For photo taking of bins
    static final int REQUEST_IMAGE_CAPTURE = 123;
    private LinearLayout imageUploader;
    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private ImageView img4;
    private ImageView currentImageViewer;
    private String[] gallery; //to store the image path
    String imageFilePath; //path to file that we are storing the photos in temporarily before sending to server

    //signature attributes
    private LinearLayout SignPadWrap;
    private LinearLayout SignPadClear;
    private ImageView clearBtn;
    private SignaturePad signaturePad;

    //FAB
    private boolean isFabOpen;
    //    private FloatingActionButton incidentButton;
    private ImageView wasteTypeBtn;
    private Animation fabOpen, fabClose, rotateForward, rotateBackward;
    public CardView reportIncidentDialog;
    private LinearLayout layoutWasteType;
    public TextView jobInfoString;

    //for API --> report incident
    private HashMap<Integer, String> allSelectedIncidents;
    private String otherIncidentDesc;
    private String incidentImageFilePath;

    //for API --> waste type
    private HashMap<Integer, String> allSelectedWasteTypes;
    private JobInfo JobInformation;

    View imageViewer;

    //location using sdk
    private boolean isSigned = false;
    GoogleMap map;
    HashMap<String, String> Parkings;
    ImageButton editParkingBtn;
    String parkingLotText;
    String parkingKey = "";
    TextView _parkTxt;
    ImageView _park;
    String currLot = "";
    ImageButton showMapBtn;
    ImageButton logoutBtn;
    ImageButton refreshLotBtn;
    RotateLoading rotateLoading;
    RotateLoading parkLoading;
    TextView name_ic;
    EditText edit_name;
    EditText edit_ic;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        String[] _ll = thisbin.getLastJobInfo().getEndLatLng().split(",");
        if(_ll.length > 1) {
            LatLng _latLng = new LatLng(Double.parseDouble(_ll[0]), Double.parseDouble(_ll[1]));
            map.addMarker(new MarkerOptions().position(_latLng).title(thisbin.getLastJobInfo().getEndLocation()));
            map.moveCamera(CameraUpdateFactory.zoomTo(15));
            map.moveCamera(CameraUpdateFactory.newLatLng(_latLng));
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bin);

        _park = findViewById(R.id.park_img);
        _parkTxt = findViewById(R.id.park_txt);
        editParkingBtn = findViewById(R.id.edit_parking);

        name_ic = findViewById(R.id.name_ic);
        edit_name = findViewById(R.id.edit_name);
        edit_ic = findViewById(R.id.edit_ic);


        logoutBtn = findViewById(R.id.logout_btn);
        logoutBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getIntent().removeExtra("loginkey");

                        //save creds to SP
                        SharedPreferences sp = getSharedPreferences("Login", MODE_PRIVATE);
                        SharedPreferences.Editor Ed = sp.edit();
                        Ed.putString("user", null);
                        Ed.putString("pass", null);
                        Ed.commit();

                        movetoLogin();
                    }
                }
        );

        Parkings = new HashMap<>();
        for(int x = 1; x < 9; x++) {
            Parkings.put("img" + (347 + x), "TA" + x);
        }
        for(int x = 1; x < 6; x++) {
            Parkings.put("img" + (355 + x), "TB" + x);
        }
        for(int x = 1; x < 7; x++) {
            Parkings.put("img" + (360 + x), "TC" + x);
        }
        for(int x = 2; x < 8; x++) {
            Parkings.put("img" + (255 + x), "A" + x);
        }
        for(int x = 2; x < 8; x++) {
            Parkings.put("img" + (262 + x), "B" + x);
        }
        for(int x = 2; x < 8; x++) {
            Parkings.put("img" + (268 + x), "C" + x);
        }
        for(int x = 1; x < 8; x++) {
            Parkings.put("img" + (275 + x), "D" + x);
        }
        for(int x = 1; x < 8; x++) {
            Parkings.put("img" + (282 + x), "E" + x);
        }
        for(int x = 1; x < 8; x++) {
            Parkings.put("img" + (289 + x), "F" + x);
        }
        Parkings.put("img297", "G1");
        Parkings.put("img298", "G7");
        Parkings.put("img299", "H1");
        Parkings.put("img300", "H7");
        Parkings.put("img301", "I1");
        Parkings.put("img302", "I7");
        Parkings.put("img303", "J1");
        Parkings.put("img304", "J7");
        for(int x = 1; x < 8; x++) {
            Parkings.put("img" + (304 + x), "K" + x);
        }
        for(int x = 1; x < 8; x++) {
            Parkings.put("img" + (311 + x), "L" + x);
        }
        for(int x = 1; x < 7; x++) {
            Parkings.put("img" + (318 + x), "M" + x);
        }
        for(int x = 1; x < 7; x++) {
            Parkings.put("img" + (324 + x), "N" + x);
        }
        for(int x = 1; x < 7; x++) {
            Parkings.put("img" + (330 + x), "O" + x);
        }
        for(int x = 1; x < 7; x++) {
            Parkings.put("img" + (337 + x), "P" + x);
        }

        if (checkLogin(savedInstanceState)) {
            // Get the Intent that started this activity and extract the string
            if (savedInstanceState == null) {
                Bundle extras = getIntent().getExtras();
                if (extras == null) {
                    BinResult = null;
                } else {
                    BinResult = extras.getString("BinResult");
                }
            } else {
                BinResult = (String) savedInstanceState.getSerializable("BinResult");
            }

            myDialog = new ProgressDialog(this);
            dialogHandler = new Handler();

            //init location text view
            textinput_location = findViewById(R.id.textinput_binlocation);
            rotateLoading = findViewById(R.id.rotateloading);
            rotateLoading.start();
            parkLoading = findViewById(R.id.parkLoading);
            showMapBtn = findViewById(R.id.show_map);
            refreshLotBtn = findViewById(R.id.refresh_lot);


            showMapBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(BinActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.dialog_custom_layout, null);
                    PhotoView photoView = mView.findViewById(R.id.imageView);
                    photoView.setImageResource(R.drawable.img_map);
                    mBuilder.setView(mView);
                    AlertDialog mDialog = mBuilder.create();
                    mDialog.show();
                }
            });

            mFusedClient = LocationServices.getFusedLocationProviderClient(this);
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(3000);
            mLocationRequest.setSmallestDisplacement(0);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationCallback = new LocationCallback() {

                @Override
                public void onLocationResult(LocationResult locationResult) {

                    if (locationResult == null) {
                        return;
                    }

                    Location newBest = null;
                    for (Location location : locationResult.getLocations()) {
                        if(newBest == null || location.getAccuracy() <= newBest.getAccuracy())
                        {
                            newBest = location;
                        }
                    }

                    if(newBest.getAccuracy() < 25) {

                        if (lastLocation == null || isBetterLocation(lastLocation, newBest)) {
                            lastLocation = newBest;
                        }

                        if (LastSentLocation != null && LastSentLocation.distanceTo(lastLocation) >= 50)
                            SendLocation = true;

                        currLat = lastLocation.getLatitude();
                        currLng = lastLocation.getLongitude();

                        try{
                            Geocoder geo = new Geocoder(BinActivity.this.getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = geo.getFromLocation(currLat, currLng, 1);
                            if (addresses.isEmpty()) {
                                //Do nothing
                                currAddress = currLat + " " + currLng + "/" + lastLocation.getAccuracy();
                                textinput_location.setText("Waiting for location...");
                            }
                            else {
                                if (addresses.size() > 0) {
                                    currAddress = addresses.get(0).getAddressLine(0);
                                    textinput_location.setText(currAddress);
                                    rotateLoading.stop();
                                }
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (HasJob) {
                            Calendar curr = Calendar.getInstance();
                            if (next == null || next.before(curr) || next.equals(curr) || SendLocation) {
                                if (lastLocation != null && currLat != null && currLat != 0) {

                                    endpoint = "UPDATEJOBLOCATION";
                                    UpdateLocation(currLat, currLng, currAddress);

                                    //lHandler.postDelayed(this, UPDATE_INTERVAL);
                                    curr.add(Calendar.SECOND, 3);
                                    next = curr;

                                    lastLocation = null;
                                }
                            }
                        }
                    }
                }
            };

            startLocationUpdates();

            View imageViewer = findViewById(R.id.imageViewer);
            btn_deploy = findViewById(R.id.btn_deploy);
            btn_collect = findViewById(R.id.btn_collect);
            btn_done = findViewById(R.id.btn_done);

            //Signature attributes. Initialize the button
            SignPadWrap = findViewById(R.id.sign_pad);
            SignPadClear = findViewById(R.id.sign_pad_clear);
            clearBtn = findViewById(R.id.close_signature_btn);
            signaturePad = findViewById(R.id.signaturePad);

            SignPadWrap.setVisibility(View.INVISIBLE);
            SignPadClear.setVisibility(View.INVISIBLE);
            gson = new Gson();

            //photo taking of bin attributes initialization
            imageUploader = findViewById(R.id.image_uploader);
            img1 = findViewById(R.id.image_view1);
            img2 = findViewById(R.id.image_view2);
            img3 = findViewById(R.id.image_view3);
            img4 = findViewById(R.id.image_view4);
            gallery = new String[4];

            img1.setOnClickListener(v -> {
                currentImageViewer = img1;
                launchCamera();
            });

            img2.setOnClickListener(v -> {
                currentImageViewer = img2;
                launchCamera();
            });

            img3.setOnClickListener(v -> {
                currentImageViewer = img3;
                launchCamera();

            });

            img4.setOnClickListener(v -> {
                currentImageViewer = img4;
                launchCamera();
            });

            //Set the image to zoom and inflate to a fragment
            img1.setOnLongClickListener(v -> {
                ImageViewerFragment dialog = new ImageViewerFragment();
                Bitmap latestImg1 = BitmapFactory.decodeFile(gallery[0]);

                if (latestImg1 == null) {
                    promptTakePhoto();
                } else {
                    dialog.bitmapImage = latestImg1;
                    dialog.show(getFragmentManager(), "ImageViewerFragment");

                }
                return true;
            });

            img2.setOnLongClickListener(v -> {
                ImageViewerFragment dialog = new ImageViewerFragment();
                Bitmap latestImg2 = BitmapFactory.decodeFile(gallery[1]);

                if (latestImg2 == null) {
                    promptTakePhoto();
                } else {
                    dialog.bitmapImage = latestImg2;
                    dialog.show(getFragmentManager(), "ImageViewerFragment");

                }
                return true;
            });

            img3.setOnLongClickListener(v -> {
                ImageViewerFragment dialog = new ImageViewerFragment();
                Bitmap latestImg3 = BitmapFactory.decodeFile(gallery[2]);

                if (latestImg3 == null) {
                    promptTakePhoto();
                } else {
                    dialog.bitmapImage = latestImg3;
                    dialog.show(getFragmentManager(), "ImageViewerFragment");

                }
                return true;
            });

            img4.setOnLongClickListener(v -> {
                ImageViewerFragment dialog = new ImageViewerFragment();
                Bitmap latestImg4 = BitmapFactory.decodeFile(gallery[3]);

                if (latestImg4 == null) {
                    promptTakePhoto();
                } else {
                    dialog.bitmapImage = latestImg4;
                    dialog.show(getFragmentManager(), "ImageViewerFragment");

                }
                return true;
            });

            clearBtn.setOnClickListener(v -> {
                signaturePad.clear();
            });

            signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                @Override
                public void onStartSigning() {

                }

                @Override
                public void onSigned() {
                    isSigned = true;
                }

                @Override
                public void onClear() {
                    isSigned = false;
                }
            });

            //FAB initialization
            wasteTypeBtn = findViewById((R.id.waste_type_btn));
            allSelectedIncidents = new HashMap<>();
            otherIncidentDesc = "";
            allSelectedWasteTypes = new HashMap<>();

            reportIncidentDialog = findViewById(R.id.report_incident_dialog);
            layoutWasteType = findViewById(R.id.layout_waste_types);
            jobInfoString = findViewById(R.id.waste_types_content);

            wasteTypeBtn.setOnClickListener(v -> {
//                if (currentJob.getJobType() == JOBTYPE_REGISTER) {
                if (thisbin.getLastJobType() == JOBTYPE_REGISTER) {
                    String Brand = thisbin.getLastJobInfo().getBrand();
                    String Model = thisbin.getLastJobInfo().getModel();
                    String SubModel = thisbin.getLastJobInfo().getSubModel();
                    String Type = thisbin.getLastJobInfo().getType();
                    String Chassis = thisbin.getLastJobInfo().getChassis();
                    String Source = thisbin.getLastJobInfo().getSource();
                    String Site = thisbin.getLastJobInfo().getSite();

                    Bundle bundle = new Bundle(); //Bundle containing data you are passing to the dialog
                    bundle.putString("Brand", Brand);
                    bundle.putString("Model", Model);
                    bundle.putString("SubModel", SubModel);
                    bundle.putString("Type", Type);
                    bundle.putString("Chassis", Chassis);
                    bundle.putString("Source", Source);
                    bundle.putString("Site", Site);

                    WasteTypeDialog wtDialog = new WasteTypeDialog();
                    wtDialog.setArguments(bundle);

                    wtDialog.show(getFragmentManager(), "WasteTypeDialog");
                } else {
                    String Brand = "";
                    String Model = "";
                    String SubModel = "";
                    String Type = "";
                    String Chassis = "";
                    String Source = "";
                    String Site = "";

                    Bundle bundle = new Bundle(); //Bundle containing data you are passing to the dialog
                    bundle.putString("Brand", Brand);
                    bundle.putString("Model", Model);
                    bundle.putString("SubModel", SubModel);
                    bundle.putString("Type", Type);
                    bundle.putString("Chassis", Chassis);
                    bundle.putString("Source", Source);
                    bundle.putString("Site", Site);

                    WasteTypeDialog wtDialog = new WasteTypeDialog();
                    wtDialog.setArguments(bundle);

                    wtDialog.show(getFragmentManager(), "WasteTypeDialog");
                }
//                } else {
//                    WasteTypeDialog wtDialog = new WasteTypeDialog();
//                    wtDialog.show(getFragmentManager(), "WasteTypeDialog");
//                }
            });
            processBinData();
        } else {
            movetoLogin();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    //take photo methods
    private boolean hasCamera() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    public void launchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            //create file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException exception) {
                Toast.makeText(this, "Unable to create file", Toast.LENGTH_LONG).show();
            }

            if (photoFile != null) { //only when file can be created
                Uri photoURI = FileProvider.getUriForFile(this, "sg.drive.myuhfreader.provider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //Take a picture and pass results along to onActivityResult
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }
        }

    }

    private void promptTakePhoto() { //when the photo view is empty and user long press it, toast will appear to prompt user to take a photo first
        Toast toast = Toast.makeText(this, getString(R.string.take_photo_prompt), Toast.LENGTH_LONG);
        ViewGroup group = (ViewGroup) toast.getView();
        TextView messageTextView = (TextView) group.getChildAt(0);
        messageTextView.setTextSize(18);
        toast.setGravity(Gravity.BOTTOM, 0, 800);
        toast.show();
    }

    private File createImageFile() throws IOException {
        String timestamp = new SimpleDateFormat("dd.MM.yyyy_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timestamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    public boolean checkLogin(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                loginkey = null;
            } else {
                loginkey = extras.getString("loginkey");
            }
        } else {
            loginkey = (String) savedInstanceState.getSerializable("loginkey");
        }

        if (loginkey == null) {
            return false;
        } else {
            return true;
        }
    }

    public void movetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void moveToScan() {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra("loginkey", loginkey);
        startActivity(intent);
    }

    private void showDialog(String msg) {
        updateDialog(msg, 0);
        myDialog.show();
    }

    private void updateDialog(String msg, int delayTime) {
        dialogHandler.postDelayed(() -> myDialog.setMessage(msg), delayTime);
    }

    private void hideDialog(int delayTime) {
        dialogHandler.postDelayed(() -> myDialog.hide(), delayTime);
    }

    @Override
    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mLocation = location;
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        return;
    }

    public void processBinData() {
        if (!BinResult.isEmpty()) {
            // set the BinResult data
            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(BinResult).getAsJsonObject();

            JsonObject bindump = o.getAsJsonObject("GetBinResult");

            thisbin = new Bin(bindump);
            rfid = StringUtil.trimDoubleQuotes(thisbin.getRFID());
            currentJob = thisbin.getJob();

            //display data
            TextView textinput_binid = findViewById((R.id.textinput_binid));
            textinput_binid.setText(thisbin.getName());

            //if empty
            String emptyTUAS = "img_empty";

            if (currentJob != null) {
                JobInfo jInfo = currentJob.getJobInfo();
                jobInfoString.setText(StringUtil.trimDoubleQuotes(jInfo.getBrand() + "," + jInfo.getModel() + "," + jInfo.getSubModel() + "," + jInfo.getType()
                        + "," + jInfo.getChassis() + "," + jInfo.getSource() + "," + jInfo.getSite()));
                if (currentJob.getJobType() == JOBTYPE_STORAGE) {
                    if (Boolean.toString(currentJob.getInTransit()) == "false") {
                        goToDeliveryPage();
                    } else if (Boolean.toString(currentJob.getInTransit()) == "true") {
                        goToStorageDONEPage();
                        HasJob = true;
                    }
                } else if (currentJob.getJobType() == JOBTYPE_DELIVER) {
                    if (Boolean.toString(currentJob.getInTransit()) == "false") {
                        goToStoragePage();
                    } else if (Boolean.toString(currentJob.getInTransit()) == "true") {
                        goToDeliveryDONEPage();
                        HasJob = true;
                    }
                } else if (currentJob.getJobType() == JOBTYPE_REGISTER) {
                    if (Boolean.toString(currentJob.getInTransit()) == "false") {
                        goToStoragePage();
                    } else if (Boolean.toString(currentJob.getInTransit()) == "true") {
                        goToStorageDONEPage();
                        HasJob = true;
                    }
                }
                if (!jInfo.getBrand().isEmpty())
                    JobInformation = jInfo;
            } else {
                goToStoragePage();
            }
        } else {
            moveToScan();
        }
    }

    public void goToStoragePage() {
        btn_deploy.setVisibility(View.VISIBLE);
        btn_collect.setVisibility(View.INVISIBLE);
        SignPadClear.setVisibility(View.INVISIBLE);
        SignPadWrap.setVisibility(View.INVISIBLE);
        SignPadClear.setVisibility(View.INVISIBLE);
        SignPadWrap.setVisibility(View.INVISIBLE);
        imageUploader.setVisibility(View.INVISIBLE);
        wasteTypeBtn.setVisibility(View.INVISIBLE);


        if (currentJob != null) {
            if (currentJob.getJobType() == JOBTYPE_REGISTER)
                jobInfoString.setVisibility(View.VISIBLE);
            else
                jobInfoString.setVisibility(View.INVISIBLE);
        } else
            jobInfoString.setVisibility(View.INVISIBLE);

        _park.setVisibility(View.GONE);
        _parkTxt.setVisibility(View.GONE);
        editParkingBtn.setVisibility(View.GONE);
        showMapBtn.setVisibility(View.GONE);
        refreshLotBtn.setVisibility(View.GONE);
    }

    public void goToDeliveryPage() {
        btn_deploy.setVisibility(View.INVISIBLE);
        btn_collect.setVisibility(View.VISIBLE);
        SignPadClear.setVisibility(View.INVISIBLE);
        SignPadWrap.setVisibility(View.INVISIBLE);
        SignPadClear.setVisibility(View.INVISIBLE);
        SignPadWrap.setVisibility(View.INVISIBLE);
        imageUploader.setVisibility(View.INVISIBLE);
        wasteTypeBtn.setVisibility(View.INVISIBLE);

        if (thisbin.getLastJobInfo().getEndLocation() == "") {
            textinput_location.setText("No previously recorded end location");
        } else {
            textinput_location.setText(thisbin.getLastJobInfo().getEndLocation());
        }


        JobInfo jInfo = thisbin.getLastJobInfo();
        jobInfoString.setText(StringUtil.trimDoubleQuotes(jInfo.getBrand() + "," + jInfo.getModel() + "," + jInfo.getSubModel() + "," + jInfo.getType()
                + "," + jInfo.getChassis() + "," + jInfo.getSource() + "," + jInfo.getSite()));

        _park.setVisibility(View.VISIBLE);
        _parkTxt.setVisibility(View.VISIBLE);
        editParkingBtn.setVisibility(View.VISIBLE);
        showMapBtn.setVisibility(View.VISIBLE);
        refreshLotBtn.setVisibility(View.VISIBLE);

        refreshLotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currLat != null) {
                    showDialog("Refreshing location...");
                    RefreshLocation(currLat, currLng, currAddress);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.location_invalid_prompt), Toast.LENGTH_LONG).show();
                }
            }
        });

        String imgPark = "img_" + StringUtil.trimDoubleQuotes(thisbin.getGeofences());
        Log.wtf("IMG", imgPark);
        int drawableResourceId = this.getResources().getIdentifier(imgPark, "drawable", this.getPackageName());
        _park.setImageResource(drawableResourceId);

        String idPark = "img" + StringUtil.trimDoubleQuotes(thisbin.getGeofences());
        String parkingText = Parkings.get(idPark);
        if (parkingText == null) {
            _parkTxt.setText("No recorded Parking Lot for this vehicle during STORAGE.");
        } else {
            _parkTxt.setText("Parking: "+parkingText);
            _park.setVisibility(View.VISIBLE);
        }

        _park.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(BinActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_custom_layout, null);
                PhotoView photoView = mView.findViewById(R.id.imageView);
                if (parkingText == null) {
                    //show nothing
                } else {
                    photoView.setImageResource(drawableResourceId);
                }
                mBuilder.setView(mView);
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });

        editParkingBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(BinActivity.this).create(); //Read Update
                alertDialog.setTitle("Change Parking Lot");

                final EditText input = new EditText(BinActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);

                alertDialog.setButton("DONE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String key = "";
                        parkingLotText = input.getText().toString().toUpperCase(); //input should be e.g. TA1 - a hashmap value
                        for(Map.Entry entry: Parkings.entrySet()){
                            if(parkingLotText.equals(entry.getValue())){
                                _park.setVisibility(View.VISIBLE);
                                parkingKey = entry.getKey().toString();
                                Log.wtf("parkingLot", parkingLotText + ", " + parkingKey);

                                //show the manual parking lot text "Parking: ___"
                                _parkTxt.setText("Parking: "+Parkings.get(parkingKey));

                                //show the manual parking lot pic
                                if (parkingKey != "" || parkingKey != null) {
                                    if (parkingKey.length() > 3)
                                    {
                                        key = parkingKey.substring(parkingKey.length() - 3);
                                        String imgParkManual = "img_" + StringUtil.trimDoubleQuotes(key);
                                        Log.wtf("IMG", imgParkManual);
                                        int parkManualResourceId = getResources().getIdentifier(imgParkManual, "drawable", getPackageName());
                                        _park.setImageResource(parkManualResourceId);
                                        _park.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AlertDialog.Builder mBuilder = new AlertDialog.Builder(BinActivity.this);
                                                View mView = getLayoutInflater().inflate(R.layout.dialog_custom_layout, null);
                                                PhotoView photoView = mView.findViewById(R.id.imageView);
                                                photoView.setImageResource(parkManualResourceId);
                                                mBuilder.setView(mView);
                                                AlertDialog mDialog = mBuilder.create();
                                                mDialog.show();
                                            }
                                        });
                                    }
                                    else
                                    {
                                        key = parkingKey;
                                    }
                                }
                                break; //breaking because its one to one map
                            }
                        }
                    }
                });
                alertDialog.setView(input);
                alertDialog.show();
            }
        });
    }

    public void goToStorageDONEPage() {
        SignPadWrap.setVisibility(View.VISIBLE);
        SignPadClear.setVisibility(View.VISIBLE);
        wasteTypeBtn.setVisibility(View.VISIBLE);
        jobInfoString.setVisibility(View.VISIBLE);
        if (thisbin.getLastJobType() != JOBTYPE_REGISTER)
            jobInfoString.setText("Please add job information");
        _parkTxt.setVisibility(View.VISIBLE);
        _parkTxt.setText("Tracking, please wait...");
        rotateLoading.start();
        parkLoading.start();
        editParkingBtn.setVisibility(View.GONE);
        showMapBtn.setVisibility(View.GONE);
        refreshLotBtn.setVisibility(View.GONE);
        _park.setVisibility(View.GONE);
        imageUploader.setVisibility(View.VISIBLE);
        btn_done.setVisibility(View.VISIBLE);
        btn_deploy.setVisibility(View.INVISIBLE);
        btn_collect.setVisibility(View.INVISIBLE);

        refreshLotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currLat != null) {
                    showDialog("Refreshing location...");
                    RefreshLocation(currLat, currLng, currAddress);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.location_invalid_prompt), Toast.LENGTH_LONG).show();
                }
            }
        });

        editParkingBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(BinActivity.this).create(); //Read Update
                alertDialog.setTitle("Change Parking Lot");

                final EditText input = new EditText(BinActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);

                alertDialog.setButton("DONE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String key = "";
                        parkingLotText = input.getText().toString().toUpperCase(); //input should be e.g. TA1 - a hashmap value
                        for(Map.Entry entry: Parkings.entrySet()){
                            if(parkingLotText.equals(entry.getValue())){
                                parkingKey = entry.getKey().toString();
                                Log.wtf("parkingLot", parkingLotText + ", " + parkingKey);

                                //show the manual parking lot text "Parking: ___"
                                _parkTxt.setText("Parking: "+Parkings.get(parkingKey));

                                //show the manual parking lot pic
                                if (parkingKey != "" || parkingKey != null) {
                                    if (parkingKey.length() > 3)
                                    {
                                        key = parkingKey.substring(parkingKey.length() - 3);
                                        String imgParkManual = "img_" + StringUtil.trimDoubleQuotes(key);
                                        Log.wtf("IMG", imgParkManual);
                                        int parkManualResourceId = getResources().getIdentifier(imgParkManual, "drawable", getPackageName());
                                        _park.setImageResource(parkManualResourceId);
                                        _park.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AlertDialog.Builder mBuilder = new AlertDialog.Builder(BinActivity.this);
                                                View mView = getLayoutInflater().inflate(R.layout.dialog_custom_layout, null);
                                                PhotoView photoView = mView.findViewById(R.id.imageView);
                                                photoView.setImageResource(parkManualResourceId);
                                                mBuilder.setView(mView);
                                                AlertDialog mDialog = mBuilder.create();
                                                mDialog.show();
                                            }
                                        });
                                    }
                                    else
                                    {
                                        key = parkingKey;
                                    }
                                }
                                break; //breaking because its one to one map
                            }
                        }
                    }
                });
                alertDialog.setView(input);
                alertDialog.show();
            }
        });
    }

    public void goToDeliveryDONEPage() {
        SignPadWrap.setVisibility(View.VISIBLE);
        SignPadClear.setVisibility(View.VISIBLE);
        wasteTypeBtn.setVisibility(View.GONE);
        _park.setVisibility(View.GONE);
        _parkTxt.setVisibility(View.GONE);
        editParkingBtn.setVisibility(View.GONE);
        showMapBtn.setVisibility(View.GONE);
        refreshLotBtn.setVisibility(View.GONE);
        parkLoading.setVisibility(View.GONE);
        imageUploader.setVisibility(View.VISIBLE);
        btn_done.setVisibility(View.VISIBLE);
        btn_deploy.setVisibility(View.INVISIBLE);
        btn_collect.setVisibility(View.INVISIBLE);
        edit_name.setVisibility(View.VISIBLE);
        edit_ic.setVisibility(View.VISIBLE);
        name_ic.setVisibility(View.VISIBLE);
    }

    //this override the implemented method from asyncTask
    @Override
    public void processFinish(String output) {
        if (endpoint == "SAVEBINJOB") {
            processResult(output);
        } else {
            Log.wtf("UPDATEBINJOB", output);
            processResult(output);
        }
    }

    public void httpRequest(String[] request) {
        //execute the async task
        MyWebRequest asyncTask = new MyWebRequest(loginkey);
        asyncTask.delegate = this;

        //requestCmd (see MyWebRequest for list of cmds)
        asyncTask.execute(request);
    }

    public void processResult(String result) {
        JsonParser parser = new JsonParser();
        JsonObject o;
        if (endpoint == "UPDATEJOBLOCATION") {
            if (currentJob != null) { // if there is current job
                if ((currentJob.getJobType() == JOBTYPE_STORAGE || currentJob.getJobType() == JOBTYPE_REGISTER)
                        && Boolean.toString(currentJob.getInTransit()) == "true") { //storage, done page
                    updateDialog("Parking lot refreshed" ,0);
                    hideDialog(500);
                    rotateLoading.stop();
                    String res;
                    o = parser.parse(result).getAsJsonObject();
                    JsonObject savejobloc = o.getAsJsonObject("SaveJobLocationResult");
                    JsonElement je = savejobloc.get("Geofences");
                    if (!je.isJsonNull()) {
                        res = firstThree(je.getAsString());
                    } else {
                        res = "";
                    }

                    Log.wtf("res", res);
                    currLot = res;

                    String key;
                    if (currLot != "" || currLot != null) {
                        if (parkingKey != "") {
                            if (parkingKey.length() > 3) {
                                key = parkingKey.substring(parkingKey.length() - 3);
                                currLot = key;
                            } else {
                                key = parkingKey;
                                currLot = key;
                            }
                        }
                        _park.setVisibility(View.VISIBLE);
                        String imgCurrPark = "img_" + StringUtil.trimDoubleQuotes(currLot);
                        int currResId = this.getResources().getIdentifier(imgCurrPark, "drawable", this.getPackageName());
                        _park.setImageResource(currResId);
                        String idCurrPark = "img" + StringUtil.trimDoubleQuotes(currLot);
                        String currParkingText = Parkings.get(idCurrPark);
                        if (currParkingText == null) {
                            _parkTxt.setText("No parking lots nearby...searching.");
                            parkLoading.start();
                            editParkingBtn.setVisibility(View.VISIBLE);
                            showMapBtn.setVisibility(View.VISIBLE);
                            refreshLotBtn.setVisibility(View.VISIBLE);
                        } else {
                            _parkTxt.setText("Parking: " + currParkingText);
                            parkLoading.stop();
                            editParkingBtn.setVisibility(View.VISIBLE);
                            showMapBtn.setVisibility(View.VISIBLE);
                            refreshLotBtn.setVisibility(View.VISIBLE);
                        }
                    } else {
                        _park.setVisibility(View.VISIBLE);
                        _parkTxt.setText("No parking lots nearby...searching.");
                        parkLoading.start();
                        editParkingBtn.setVisibility(View.VISIBLE);
                        showMapBtn.setVisibility(View.VISIBLE);
                        refreshLotBtn.setVisibility(View.VISIBLE);
                    }
                } else if (currentJob.getJobType() == JOBTYPE_STORAGE && Boolean.toString(currentJob.getInTransit()) == "false") {
                    String res;
                    o = parser.parse(result).getAsJsonObject();
                    JsonObject savejobloc = o.getAsJsonObject("SaveJobLocationResult");
                    updateDialog("Parking lot refreshed" ,0);
                    hideDialog(500);
                    JsonElement je = savejobloc.get("Geofences");
                    if (!je.isJsonNull()) {
                        res = firstThree(je.getAsString());
                    } else {
                        res = "";
                    }

                    Log.wtf("res", res);
                    currLot = res;

                    String key;
                    if (currLot != "" || currLot != null) {
                        if (parkingKey != "") {
                            if (parkingKey.length() > 3) {
                                key = parkingKey.substring(parkingKey.length() - 3);
                                currLot = key;
                            } else {
                                key = parkingKey;
                                currLot = key;
                            }
                        }
                        _park.setVisibility(View.VISIBLE);
                        String imgCurrPark = "img_" + StringUtil.trimDoubleQuotes(currLot);
                        int currResId = this.getResources().getIdentifier(imgCurrPark, "drawable", this.getPackageName());
                        _park.setImageResource(currResId);
                        String idCurrPark = "img" + StringUtil.trimDoubleQuotes(currLot);
                        String currParkingText = Parkings.get(idCurrPark);
                        if (currParkingText == null) {
                            _parkTxt.setText("No parking lots nearby. Please try again.");
                            editParkingBtn.setVisibility(View.VISIBLE);
                            showMapBtn.setVisibility(View.VISIBLE);
                            refreshLotBtn.setVisibility(View.VISIBLE);
                        } else {
                            _parkTxt.setText("Parking: " + currParkingText);
                            editParkingBtn.setVisibility(View.VISIBLE);
                            showMapBtn.setVisibility(View.VISIBLE);
                            refreshLotBtn.setVisibility(View.VISIBLE);
                        }
                    } else {
                        _park.setVisibility(View.VISIBLE);
                        _parkTxt.setText("No parking lots nearby. Please try again.");
                        editParkingBtn.setVisibility(View.VISIBLE);
                        showMapBtn.setVisibility(View.VISIBLE);
                        refreshLotBtn.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        else if (endpoint == "GETREVERSEGEOCODE") {
            o = parser.parse(result).getAsJsonObject();
            JsonElement je = o.get("GetReverseGeocodeResult");
            if (!je.isJsonNull()) {
                String address = je.getAsString();
                if (address != null) {
                    Log.wtf("SHORT ADDRESS", address);
                    textinput_location.setText(address);
                }
            }
        }
        else if (endpoint == "SAVEBINJOB") {
            o = parser.parse(result).getAsJsonObject();
            JsonObject savebin = o.getAsJsonObject("SaveBinJobResult");
            JsonElement je = savebin.get("Message");
            String res = je.getAsString();
            if (res.contains("OK")) {
                //get job data before moving to confirmation
                JsonElement json_jobid = savebin.get("Data");
                String jid = json_jobid.getAsString();
                if (jid != "0") {
                    Log.wtf("SAVE BIN JOB", jid);
                    currentJob.setJobID(Integer.parseInt(jid));
                    parkLoading.start();

                    if (currentJob.getJobType() == JOBTYPE_DELIVER) { //current job is collection / delivery
                        updateDialog("Delivery started" ,0);
                        hideDialog(1000);

                        JobInfo jInfo = currentJob.getJobInfo();
                        goToDeliveryDONEPage();

                        jobInfoString.setText(
                                jInfo.getBrand() + "," +
                                jInfo.getModel() + "," +
                                jInfo.getSubModel() + "," +
                                jInfo.getType() + "," +
                                jInfo.getChassis() + "," +
                                jInfo.getSource() + "," +
                                jInfo.getSite()
                        );

                    } else if (currentJob.getJobType() == JOBTYPE_STORAGE) { //deployment / storage , done page
                        updateDialog("Storage started" ,0);
                        hideDialog(1000);
                        goToStorageDONEPage();
                    }
                    HasJob = true;
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.oops_error), Toast.LENGTH_LONG).show();
                }
            }
        }
        else if (endpoint == "UPDATEBINJOB") {
            if (!result.isEmpty()) {
                o = parser.parse(result).getAsJsonObject();
                if (!o.has("UpdateBinJobResult")) {

                } else {
                    Log.wtf("HAS", result);
                    updateDialog(getString(R.string.job_complete), 0);
                    dialogHandler.postDelayed(() -> moveToScan(), 3000);
                }
            } else {
                hideDialog(1000);
            }
        } else if(endpoint == "REPARK") {
            if (!result.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Vehicle has been REPARKED", Toast.LENGTH_LONG).show();
                updateDialog("Vehicle has been REPARKED", 0);
                dialogHandler.postDelayed(() -> moveToScan(), 3000);
            } else {
                hideDialog(1000);
            }
        }
    }

    public String firstThree(String str) {
        return str.length() < 3 ? str : str.substring(0, 3);
    }

    public void confirmDeploy(View view) {
        DeployDialogFragment deployconfirm = new DeployDialogFragment();
        deployconfirm.show(getFragmentManager(), "deployconfirm");
    }

    public void confirmRepark(View view) {
        ReparkDialogFragment reparkconfirm = new ReparkDialogFragment();
        reparkconfirm.show(getFragmentManager(), "reparkconfirm");
    }

    public void confirmCollect(View view) {

        CollectDialogFragment collectconfirm = new CollectDialogFragment();
        collectconfirm.show(getFragmentManager(), "collectconfirm");
    }

    public void confirmDone(View view) {
        Log.wtf("name", edit_name.getText().toString());
        String name = edit_name.getText().toString();
        String ic = edit_ic.getText().toString();

        // add confirmation info
        if(!isSigned)
        {
            Toast.makeText(getApplicationContext(), "Please sign before proceeding", Toast.LENGTH_LONG).show();
            return;
        }
        else if (ic.length() != 9 && currentJob.getJobType() == 1 && Boolean.toString(currentJob.getInTransit()) == "true") {
            Toast.makeText(getApplicationContext(), "Please enter a valid NRIC", Toast.LENGTH_LONG).show();
            return;
        }
        else if (name.length() == 0 && currentJob.getJobType() == 1 && Boolean.toString(currentJob.getInTransit()) == "true") {
            Toast.makeText(getApplicationContext(), "Please enter your full name", Toast.LENGTH_LONG).show();
            return;
        }
        else if(gallery[0] == null && gallery[1] == null && gallery[2] == null && gallery[3] == null)
        {
            Toast.makeText(getApplicationContext(), "Please take a photo before proceeding", Toast.LENGTH_LONG).show();
            return;
        }
        else if((JobInformation == null) && currentJob.getJobType() == 0)
        {
            Toast.makeText(getApplicationContext(), "Please add the job info before proceeding", Toast.LENGTH_LONG).show();
            return;
        }
        DoneDialogFragment doneconfirm = new DoneDialogFragment();
        doneconfirm.show(getFragmentManager(), "doneconfirm");
    }

    @Override
    public void deployOk(DialogFragment dialog) {
        modeselected = "DEPLOY";
        saveBinJob();
        if (currLat != null) {
            showDialog("Starting storage...");
        } else {
            //do nothing
        }
    }

    @Override
    public void collectNOK(DialogFragment dialog) {
        //do nothing
    }

    @Override
    public void collectOk(DialogFragment dialog) {
        modeselected = "COLLECT";
        saveBinJob();
        if (currLat != null) {
            showDialog("Starting delivery...");
        } else {
            //do nothing
        }
    }

    @Override
    public void reparkOk(DialogFragment dialog) {
        if(currLat != null){
            showDialog("Reparking vehicle...");
            currentJob = new Job();
            currentJob.setJobID(thisbin.getLastJobID());
            currentJob.setInTransit(false);
            currentJob.setJobType(1);
            //TODO: handle job type
            endpoint = "REPARK";
            //set update end date
            currentJob.setEnd(getCurrentDate());
            currentJob.setBinID(thisbin.getBinID());

            String key = "";
            if (parkingKey.length() > 3)
            {
                key = parkingKey.substring(parkingKey.length() - 3);
                currentJob.setGeofences(key);
            }
            else
            {
                key = parkingKey;
            }

            // get new location
            //add EndPosition from device
            Map<String, Double> ep = new HashMap<>();
            ep.put("Latitude", currLat);
            ep.put("Longitude", currLng);
            currentJob.setEndPosition(ep);

            // JSON data structure
            JsonElement jsonElement = gson.toJsonTree(currentJob);
            JsonObject jsonObject = (JsonObject) jsonElement;

            // property removal
            jsonObject.remove("property");

            //remove StartPosition (not required by endpoint)
            jsonObject.remove("StartPosition");

            // serialization to String
            String javaObjectString = jsonObject.toString();
            Log.wtf("REPARK", javaObjectString);

            // http request to update bin job
            String[] bin_data = {endpoint, javaObjectString};
            httpRequest(bin_data);

            updateDialog("Reparking vehicle...", 0);
        }
        else
        {
            updateDialog("Location not found, please try again.", 0);
            hideDialog(1000);
        }
    }

    @Override
    public void reparkNOK(DialogFragment dialog) {

    }

    @Override
    public void deployNOK(DialogFragment dialog) {
        //do nothing
    }

    @Override
    public void doneOk(DialogFragment dialog) {
        showDialog("Updating job");
        endpoint = "UPDATEBINJOB";
        HasJob = false;
        processJob();
    }

    @Override
    public void doneNOK(DialogFragment dialog) {
        // nothing
    }


    private void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_CODE_FINE_LOCATION);

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_REQUEST_CODE_COARSE_LOCATION);

                return;
            }

            mFusedClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback,
                    null /* Looper */);
        }
        catch(Exception e)
        {
            Log.wtf("s", e.getMessage());
        }
    }

    private void stopLocationUpdates() {
        mFusedClient.removeLocationUpdates(mLocationCallback);
    }

    public void RefreshLocation(double lat, double lng, String add) {
        parkingKey = "";
        currLot = "";
        endpoint = "UPDATEJOBLOCATION";
        updateDialog("Retrieving parking lot...", 0);
        JobLocation jobLocation = new JobLocation();

        jobLocation.setJobID(thisbin.getLastJobID());
        jobLocation.setLastSeen(getCurrentDate());
        jobLocation.setLocation(add);

        HashMap<String, Double> jobloc = new HashMap<>();
        jobloc.put("Latitude", lat);
        jobloc.put("Longitude", lng);
        jobLocation.setPosition(jobloc);

        JsonElement jsonElement = gson.toJsonTree(jobLocation);
        JsonObject jsonObject = (JsonObject) jsonElement;

        String javaObjectString = jsonObject.toString();
        Log.wtf("refreshloc", javaObjectString);

        String[] bin_data = {endpoint, javaObjectString};
        httpRequest(bin_data);
    }

    @Override
    public void UpdateLocation(LocationInfo locationInfo) {
        textinput_location.setText(currAddress);

        if(endpoint == ""){}
        else if(endpoint == "UPDATEJOBLOCATION") {
            JobLocation jobLocation = new JobLocation();

            jobLocation.setJobID(currentJob.getJobID());
            jobLocation.setLastSeen(getCurrentDate());
            jobLocation.setLocation(currAddress);

            HashMap<String, Double> jobloc = new HashMap();
            jobloc.put("Latitude", currLat);
            jobloc.put("Longitude", currLng);
            jobLocation.setPosition(jobloc);

            JsonElement jsonElement = gson.toJsonTree(jobLocation);
            JsonObject jsonObject = (JsonObject) jsonElement;

            String javaObjectString = jsonObject.toString();

            String[] bin_data = {endpoint, javaObjectString};
            httpRequest(bin_data);
        } else {
            //TODO: handle job type

            //set InTransit to false
            currentJob.setInTransit(false);

            //set update end date
            currentJob.setEnd(getCurrentDate());
            // get new location
            //add EndPosition from device
            Map<String, Double> ep = new HashMap<String, Double>();
            ep.put("Latitude", currLat);
            ep.put("Longitude", currLng);
            currentJob.setEndPosition(ep);

            //add signature
            Bitmap bmp2 = signaturePad.getSignatureBitmap();

            double nHeight =  bmp2.getHeight();
            double nWidth =  bmp2.getWidth();      // resize the image by 400xscale
            Bitmap bmp = Bitmap.createScaledBitmap(bmp2, 400, 200, true);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            String signBase64 = android.util.Base64.encodeToString(byteArray, android.util.Base64.NO_WRAP);
            bmp.recycle();

            Photos images = new Photos();
            images.setSignature(signBase64);
            Log.wtf("Signbase64", signBase64);
            currentJob.setPhotos(images);


            JobInformation.setDriverName(edit_name.getText().toString());
            JobInformation.setIDNumber(edit_ic.getText().toString());
            currentJob.setJobInfo(JobInformation);

            // JSON data structure
            JsonElement jsonElement = gson.toJsonTree(currentJob);
            JsonObject jsonObject = (JsonObject) jsonElement;
            // property removal
            jsonObject.remove("property");

            //remove StartPosition (not required by endpoint)
            jsonObject.remove("StartPosition");

            // serialization to String
            String javaObjectString = jsonObject.toString();
            Log.wtf("UPDATE JOB", javaObjectString);


            // http request to update bin job
            String[] bin_data = {endpoint, javaObjectString};
            httpRequest(bin_data);
        }
    }

    public void UpdateLocation(double lat, double lng, String add) {
        if(endpoint == ""){}
        else if(endpoint == "UPDATEJOBLOCATION") {
            JobLocation jobLocation = new JobLocation();

            jobLocation.setJobID(currentJob.getJobID());
            jobLocation.setLastSeen(getCurrentDate());
            jobLocation.setLocation(add);

            HashMap<String, Double> jobloc = new HashMap<>();
            jobloc.put("Latitude", lat);
            jobloc.put("Longitude", lng);
            jobLocation.setPosition(jobloc);

            JsonElement jsonElement = gson.toJsonTree(jobLocation);
            JsonObject jsonObject = (JsonObject) jsonElement;

            String javaObjectString = jsonObject.toString();

            String[] bin_data = {endpoint, javaObjectString};
            httpRequest(bin_data);
        } else {
            //TODO: handle job type
            JsonParser parser = new JsonParser();
            for (String ph: gallery) {
                if(ph != null) {
                    Bitmap imgBt = BitmapFactory.decodeFile(ph);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    double height = imgBt.getHeight() - (imgBt.getHeight() * .97);
                    double width = imgBt.getWidth() - (imgBt.getWidth() * .97);
                    imgBt = Bitmap.createScaledBitmap(imgBt, (int)width, (int)height, false);
                    imgBt.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String encoded = android.util.Base64.encodeToString(byteArray, android.util.Base64.DEFAULT);

                    String payload = "{}";
                    JsonObject o = parser.parse(payload).getAsJsonObject();
                    o.add("JobID", new Gson().toJsonTree(currentJob.getJobID()));
                    o.add("Stream", new Gson().toJsonTree(encoded));

                    String phjsonstring = o.toString();

                    // http request to update bin job
                    String[] bin_data = {"ADDJOBPHOTOS", phjsonstring};
                    httpRequest(bin_data);
                }
            }

            //set InTransit to false
            currentJob.setInTransit(false);

            //set job type
            if (currentJob.getJobType() == 0)
                currentJob.setJobType(1);
            else if (currentJob.getJobType() == 1) {
                currentJob.setJobType(0);
            } else if (currentJob.getJobType() == 3) {
                currentJob.setJobType(1);
            }

            //set update end date
            currentJob.setEnd(getCurrentDate());
            // get new location
            //add EndPosition from device
            Map<String, Double> ep = new HashMap<String, Double>();
            ep.put("Latitude", lat);
            ep.put("Longitude", lng);
            currentJob.setEndPosition(ep);

            String key = "";
            if (parkingKey.length() > 3)
            {
                key = parkingKey.substring(parkingKey.length() - 3);
                currentJob.setGeofences(key);
            }
            else
            {
                key = parkingKey;
            }

            // set job info
            JobInformation.setDriverName(edit_name.getText().toString());
            JobInformation.setIDNumber(edit_ic.getText().toString());
            currentJob.setJobInfo(JobInformation);

            //add signature
            Bitmap bmp2 = signaturePad.getSignatureBitmap();

            double nHeight =  bmp2.getHeight();
            double nWidth =  bmp2.getWidth();      // resize the image by 400xscale
            Bitmap bmp = Bitmap.createScaledBitmap(bmp2, 400, 200, true);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            String signBase64 = android.util.Base64.encodeToString(byteArray, android.util.Base64.NO_WRAP);
            bmp.recycle();

            Photos images = new Photos();
            images.setSignature(signBase64);
            Log.wtf("Signbase64", signBase64);
            currentJob.setPhotos(images);

            // JSON data structure
            JsonElement jsonElement = gson.toJsonTree(currentJob);
            JsonObject jsonObject = (JsonObject) jsonElement;
            // property removal
            jsonObject.remove("property");

            //remove StartPosition (not required by endpoint)
            jsonObject.remove("StartPosition");

            // serialization to String
            String javaObjectString = jsonObject.toString();
            Log.wtf("UPDATE JOB", javaObjectString);

            // http request to update bin job
            String[] bin_data = {endpoint, javaObjectString};
            httpRequest(bin_data);
        }
    }

    protected String ObjectToJSON(Object ob){
        // JSON data structure
        JsonElement jsonElement = gson.toJsonTree(ob);
        JsonObject jsonObject = (JsonObject) jsonElement;

        // property removal
        jsonObject.remove("property");
        // serialization to String
        String javaObjectString = jsonObject.toString();
        //Log.wtf("JSON STRING", javaObjectString);

        return javaObjectString;
    }

    private String getCurrentDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String cdate = dateFormat.format(new Date());
        return cdate;
    }

    private void saveBinJob(){
        //save current location when creating new job
        if(currLat != null && currLng != null && rfid != null){

            currentJob = new Job();
            currentJob.setInTransit(true);
            currentJob.setJobType(modeselected == "DEPLOY" ? 0 : 1);
            currentJob.setStart(getCurrentDate());
            currentJob.setRFID(rfid);
            if(currentJob.getJobType() == 1)
                currentJob.setJobInfo(thisbin.getLastJobInfo());

            Integer[] wList = new Integer[allSelectedWasteTypes.size()];

            Iterator<Integer> itr = allSelectedWasteTypes.keySet().iterator();
            Integer i = 0;
            while(itr.hasNext())
            {
                Integer k = itr.next();
                wList[i] = k;
                i++;
            }

            currentJob.setWasteTypes(wList);

            Map<String, Double> StartPosition = new HashMap<String, Double>();
            StartPosition.put("Latitude", currLat);
            StartPosition.put("Longitude", currLng);
            currentJob.setStartPosition(StartPosition);

            String key = "";
            if (parkingKey.length() > 3) {
                key = parkingKey.substring(parkingKey.length() - 3);
                currentJob.setGeofences(key);
            } else {
                key = parkingKey;
                currentJob.setGeofences(key);
            }


            String jsonstring = ObjectToJSON(currentJob);

            // http request to update bin job
            endpoint = "SAVEBINJOB";
            String[] bin_data = {endpoint, jsonstring};
            httpRequest(bin_data);
        }else{
            Toast.makeText(getApplicationContext(), getString(R.string.location_invalid_prompt), Toast.LENGTH_LONG).show();
        }
    }

    private void processJob(){
        if(currentJob != null && currLat != null){
            //locHelper.getLocation();
            UpdateLocation(currLat, currLng, currAddress);
        }
        else
        {
            updateDialog("Location not found, please try again.", 0);
            hideDialog(1000);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == CONFIRM_JOB) {
            // Make sure the request was successful
            if (resultCode == CONFIRM_BACK_PRESSED) {
                //get bin data
                processBinData();
            }
        }

        //return image taken
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
//            Bitmap photo = BitmapFactory.decodeFile(imageFilePath);
            switch(currentImageViewer.getId())
            {
                case R.id.image_view1:
                    gallery[0] = imageFilePath;
                    break;

                case R.id.image_view2:
                    gallery[1] = imageFilePath;
                    break;

                case R.id.image_view3:
                    gallery[2] = imageFilePath;
                    break;

                case R.id.image_view4:
                    gallery[3] = imageFilePath;
                    break;
            }

            //get the image view's dimensions
            int viewW = currentImageViewer.getWidth();
            int viewH = currentImageViewer.getHeight();

            //get bitmap's dimensions
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imageFilePath,bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            //how much to scale down the image
            int scaleFactor = Math.min(photoW/viewW, photoH/viewH);

            //Decode image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap btp = BitmapFactory.decodeFile(imageFilePath,bmOptions);
            currentImageViewer.setImageBitmap(btp);
        }
    }

    public void saveWasteTypesInstance(JobInfo jInfo){ //initialize all the wastes selected for the API
        jobInfoString.setText(
                jInfo.getBrand() + "," +
                        jInfo.getModel() + "," +
                        jInfo.getSubModel() + "," +
                        jInfo.getType() + "," +
                        jInfo.getChassis() + "," +
                        jInfo.getSource() + "," +
                        jInfo.getSite()
        );
        JobInformation = jInfo; //for api
    }

    public void saveIncidentDescInstance(HashMap<Integer, String> selectedIncidents, String otherDesc){ //initialize all the incidents reported for the API
        allSelectedIncidents = selectedIncidents; //used for api
        otherIncidentDesc = otherDesc; //for api
    }

    public void saveImageFilePath(String path){
        incidentImageFilePath = path;
    }

    static final int TIME_DIFFERENCE_THRESHOLD = 3 * 1000;
    boolean isBetterLocation(Location oldLocation, Location newLocation) {
        // If there is no old location, of course the new location is better.
        if(oldLocation == null) {
            return true;
        }

        // Check if new location is newer in time.
        boolean isNewer = newLocation.getTime() > oldLocation.getTime();

        if(isNewer) {
            // More accurate and newer is always better.
            return true;
        }

        return false;
    }
}

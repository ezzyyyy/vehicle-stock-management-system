package sg.drive.myuhfreader;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rscja.deviceapi.RFIDWithUHF;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements AsyncResponse {
    String endpoint;
    String user;
    String pass;

    //this override the implemented method from asyncTask
    @Override
    public void processFinish(String output){
        //Here you will receive the result fired from async class
        //of onPostExecute(result) method.
        processResult(output);
        //Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();
    }

    public void httpRequest(String[] request) {
        //execute the async task
        MyWebRequest asyncTask = new MyWebRequest("");
        asyncTask.delegate = this;

        //requestCmd (see MyWebRequest for list of cmds)
        asyncTask.execute(request);
    }

    public void processResult(String result){
        if(result.isEmpty())
        {
            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.login_failed_prompt), Toast.LENGTH_LONG);
            ViewGroup group = (ViewGroup) toast.getView();
            TextView messageTextView = (TextView) group.getChildAt(0);
            messageTextView.setTextSize(19);
            toast.show();
        }
        else {
            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(result).getAsJsonObject();
            if (endpoint == "LOGIN") {
                JsonElement je = o.get("LogInResult");
                String loginkey = je.getAsString();
                if (loginkey != null) {
                    Log.wtf("LOGIN KEY", loginkey);
                    //go to scan
                    movetoScan(loginkey);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.invalid_credentials_prompt, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public void movetoScan(String loginkey){
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra("loginkey", loginkey);

        //save creds to SP
        SharedPreferences sp = getSharedPreferences("Login", MODE_PRIVATE);
        SharedPreferences.Editor Ed = sp.edit();
        Ed.putString("user", user);
        Ed.putString("pass", pass);
        Ed.commit();

        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        SharedPreferences sp = getSharedPreferences("Login", MODE_PRIVATE);
        user = sp.getString("user", null);
        pass = sp.getString("pass", null);
        checkAuth(user, pass);
    }

    public void checkAuth(String user, String pass) {
        if(user != null && pass != null){
            attemptLogin(user, pass);
        }else{
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    private void attemptLogin(String user, String pass) {
        //create payload
        String payload = "{}";

        //convert jsonstring back to json object
        JsonParser parser = new JsonParser();
        JsonObject o = parser.parse(payload).getAsJsonObject();

        //add apikey to json object
        o.add("Username", new Gson().toJsonTree(user));
        o.add("Password", new Gson().toJsonTree(pass));
        String postDataParams = o.toString();
        endpoint = "LOGIN";
        String[] login_data = {endpoint, postDataParams};
        httpRequest(login_data);
    }
}

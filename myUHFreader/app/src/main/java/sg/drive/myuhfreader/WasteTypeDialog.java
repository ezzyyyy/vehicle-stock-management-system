package sg.drive.myuhfreader;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.checkerframework.checker.linear.qual.Linear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class WasteTypeDialog extends DialogFragment{
    private static final String TAG = "WasteTypeDialog";
    public String desc; //the selected waste types + any extra comments/descriptions entered by the driver
    private Button wasteTypeSubmitBtn; //the button to dimiss the fragment + send all waste typeSpinner desc to BinActivity

    List<String> Brands = new ArrayList<>();
    List<String> Models;
    List<String> SubModels;

    Spinner brandSpinner;
    Spinner modelSpinner;
    Spinner submodelSpinner;
    Spinner typeSpinner;
    Spinner sourceSpinner;
    Spinner siteSpinner;

    TextView chassisInput;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        Context _cont = this.getContext();
        View view = inflater.inflate(R.layout.waste_type_fragment, container, false);

        Bundle bundle = getArguments();
        String Brand = bundle.getString("Brand","-");
        String Model = bundle.getString("Model","-");
        String SubModel = bundle.getString("SubModel","-");
        String Type = bundle.getString("Type","-");
        String Chassis = bundle.getString("Chassis","Enter Vehicle Chassis");
        String Source = bundle.getString("Source","-");
        String Site = bundle.getString("Site","-");

        desc = "";
        wasteTypeSubmitBtn = view.findViewById(R.id.waste_type_submit);

        brandSpinner = view.findViewById(R.id.brandSpinner);
        modelSpinner = view.findViewById(R.id.modelSpinner);
        submodelSpinner = view.findViewById(R.id.submodelSpinner);
        typeSpinner = view.findViewById(R.id.typeSpinner);
        chassisInput = view.findViewById(R.id.chassisInput);
        sourceSpinner = view.findViewById(R.id.sourceSpinner);
        siteSpinner = view.findViewById(R.id.siteSpinner);

        wasteTypeSubmitBtn.setOnClickListener(v->{ //upon submit, send the description of the waste types to bin activity
            JobInfo jobinfo = new JobInfo();
            jobinfo.setBrand(brandSpinner.getSelectedItem().toString());
            jobinfo.setModel(modelSpinner.getSelectedItem().toString());
            jobinfo.setSubModel(submodelSpinner.getSelectedItem().toString());
            jobinfo.setType(typeSpinner.getSelectedItem().toString());
            jobinfo.setChassis(chassisInput.getText().toString());
            jobinfo.setSource(sourceSpinner.getSelectedItem().toString());
            jobinfo.setSite(siteSpinner.getSelectedItem().toString());
            ((BinActivity)getActivity()).saveWasteTypesInstance(jobinfo);
            dismiss();
        });

        ArrayList<String> TypeList = new ArrayList();
        ArrayList<String> SourceList = new ArrayList();
        ArrayList<String> SiteList = new ArrayList();

        TypeList.add("10 Wheeler");
        TypeList.add("10ft Truck");
        TypeList.add("14ft Truck");
        TypeList.add("16ft Truck");
        TypeList.add("24ft truck");
        TypeList.add("Bus");
        TypeList.add("Panel Vans");
        TypeList.add("Others");

        SourceList.add("IG");
        SourceList.add("Goldbell");
        SourceList.add("Mercedes");
        SourceList.add("Wong Fong");
        SourceList.add("Syntech");
        SourceList.add("Others");

        SiteList.add("JLH");
        SiteList.add("TUAS");

        ArrayAdapter<String> typeArrayAdapter = new ArrayAdapter(_cont, R.layout.spinner, TypeList);
        ArrayAdapter<String> sourceArrayAdapter = new ArrayAdapter(_cont, R.layout.spinner, SourceList);
        ArrayAdapter<String> siteArrayAdapter = new ArrayAdapter(_cont, R.layout.spinner, SiteList);

        typeSpinner.setAdapter(typeArrayAdapter);
        sourceSpinner.setAdapter(sourceArrayAdapter);
        siteSpinner.setAdapter(siteArrayAdapter);

        typeSpinner.setSelection(TypeList.indexOf(Type));
        sourceSpinner.setSelection(SourceList.indexOf(Source));
        siteSpinner.setSelection(SiteList.indexOf(Site));
        chassisInput.setText(Chassis);

        String[] BrandList = "Mercedes Benz,Mitsubishi,Fiat,Volvo".split(",");
        String[] MZModelList = "Aroc,New Atego,Sprinter/3665,Sprinter/4325,Actros,Vito,V-Class,Citan".split(",");
        String[] MBModelList = ("Fuso/FV 70HJA,Fuso/FK 62FMA,Fuso/FV 70HSA,Fuso/FQ 62FSA,Fuso/FP 74HD,Fuso/FS 72HSA,Fuso/FV 70HKA,Fuso/FP 70HDA,Fuso/ROSA," +
                "Canter/FEB 21EA,Canter/FEB 21CA,Canter/FEB 71GA,Canter/FEB 51EA,Canter/FEB 71EA,Canter/FEC X1HA,Canter/FEA 01BA").split(",");
        String[] VVModelList = "Trucks/Volvo FH16,Trucks/Volvo FH,Trucks/Volvo FMX,Trucks/Volvo FM,Trucks/Volvo FE,Trucks/Volvo FL".split(",");
        String[] FiModelList = "Fiorino,Doblo".split(",");
        for (String s: BrandList)
            Brands.add(s);

        ArrayAdapter<String> brandArrayAdapter = new ArrayAdapter(_cont, R.layout.spinner, Brands);

        brandSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                Models = new ArrayList<>();
                SubModels = new ArrayList<>();
                String bSel = Brands.get(position);
                String[] ModelList = new String[20];
                switch(bSel)
                {
                    case "Mercedes Benz":
                        ModelList = MZModelList;
                        break;
                    case "Mitsubishi":
                        ModelList = MBModelList;
                        break;
                    case "Volvo":
                        ModelList = VVModelList;
                        break;
                    case "Fiat":
                        ModelList = FiModelList;
                        break;
                }

                for (String md: ModelList) {
                    String[] _md = md.split("/");
                    if(!Models.contains(_md[0])) {
                        Models.add(_md[0]);
                    }

                    if(_md.length > 1)
                        SubModels.add(md);
                }

                ArrayAdapter<String> modelArrayAdapter = new ArrayAdapter(_cont, R.layout.spinner, Models);

                modelSpinner.setAdapter(modelArrayAdapter);
                modelSpinner.setSelection(Models.indexOf(Model));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        modelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String bSel = modelSpinner.getSelectedItem().toString();
                List<String> SubModelsSel = new ArrayList<>();
                SubModelsSel.add("None");
                for (String md: SubModels) {
                    String[] _md = md.split("/");
                    if(bSel.equals(_md[0])) {
                        SubModelsSel.add(_md[1]);
                    }
                }

                ArrayAdapter<String> submodelArrayAdapter = new ArrayAdapter(_cont, R.layout.spinner, SubModelsSel);

                submodelSpinner.setAdapter(submodelArrayAdapter);
                submodelSpinner.setSelection(SubModelsSel.indexOf(SubModel));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        brandSpinner.setAdapter(brandArrayAdapter);
        brandSpinner.setSelection(Brands.indexOf(Brand));

        return view;
    }

    @Override
    public void dismiss(){
        super.dismiss();
    }


}

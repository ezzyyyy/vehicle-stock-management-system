package sg.drive.myuhfreader;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class JobInfo {
    private Integer JobID;
    private String Brand; //the RFID tag ID
    private String Model; //the bin current location
    private String SubModel; //status of the bin (deployed, under maintenance, ready for deployment)
    private String Type;
    private String Chassis;
    private String IDNumber;
    private String DriverName;
    private String Source;
    private String Site;
    private String EndLatLng;
    private String EndLocation;

    private Integer JobType;
    public JobInfo(){
        super(); //Default constructor: inherits the constructor of direct parent class, Object
    }

    public JobInfo(JsonObject jobdata) { //constructor using bindata
        this.JobID = Integer.parseInt(jobdata.get("LastJobID").toString()); //obtain binID in obj format then convert to string then integer
        this.Brand = StringUtil.trimDoubleQuotes(jobdata.get("Brand").toString());
        this.Model = StringUtil.trimDoubleQuotes(jobdata.get("Model").toString());
        this.SubModel = StringUtil.trimDoubleQuotes(jobdata.get("SubModel").toString());
        this.Type = StringUtil.trimDoubleQuotes(jobdata.get("Type").toString());
        this.Chassis = StringUtil.trimDoubleQuotes(jobdata.get("Chassis").toString());
        this.IDNumber = StringUtil.trimDoubleQuotes(jobdata.get("IDNumber").toString());
        this.DriverName = StringUtil.trimDoubleQuotes(jobdata.get("DriverName").toString());
        this.Source = StringUtil.trimDoubleQuotes(jobdata.get("Source").toString());
        this.Site = StringUtil.trimDoubleQuotes(jobdata.get("Site").toString());
        this.JobType = Integer.parseInt(jobdata.get("LastJobType").toString());

        this.EndLocation = StringUtil.trimDoubleQuotes(jobdata.get("LastJobEndLocation").toString());
        this.EndLatLng = StringUtil.trimDoubleQuotes(jobdata.get("LastJobEndLatLng").toString());
    }

    public Integer getJobID() {
        return JobID;
    }

    public String getBrand() {
        return Brand;
    }

    public String getChassis() {
        return Chassis;
    }

    public String getIDNumber() {
        return IDNumber;
    }

    public String getDriverName() {
        return DriverName;
    }

    public String getModel() {
        return Model;
    }

    public String getSite() {
        return Site;
    }

    public String getSource() { return Source; }

    public String getSubModel() {
        return SubModel;
    }

    public String getType() {
        return Type;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public void setChassis(String chassis) {
        Chassis = chassis;
    }

    public void setIDNumber(String idNumber) {
        IDNumber = idNumber;
    }

    public void setDriverName(String driverName) {
        DriverName = driverName;
    }

    public void setJobID(Integer jobID) {
        JobID = jobID;
    }

    public void setModel(String model) {
        Model = model;
    }

    public void setSite(String site) {
        Site = site;
    }

    public void setSource(String source) {
        Source = source;
    }

    public void setSubModel(String subModel) {
        SubModel = subModel;
    }

    public void setType(String type) {
        Type = type;
    }

    public Integer getJobType() {
        return JobType;
    }

    public void setJobType(Integer jobType) {
        JobType = jobType;
    }

    public void setEndLatLng(String endLatLng) {
        EndLatLng = endLatLng;
    }

    public String getEndLatLng() {
        return EndLatLng;
    }

    public void setEndLocation(String endLocation) {
        EndLocation = endLocation;
    }

    public String getEndLocation() {
        return EndLocation;
    }
}

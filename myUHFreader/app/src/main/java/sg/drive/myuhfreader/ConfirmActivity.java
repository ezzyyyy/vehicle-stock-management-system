package sg.drive.myuhfreader;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ConfirmActivity extends AppCompatActivity
        implements AsyncResponse,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ConfirmDialogFragment.ConfirmDialogListener {

    //GPS variables
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener listener;
    private long UPDATE_INTERVAL = 5 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private static final int PERMISSION_REQUEST_CODE_FINE_LOCATION = 1;
    private static final int PERMISSION_REQUEST_CODE_COARSE_LOCATION = 2;
    TextInputEditText confirm_lat;
    TextInputEditText confirm_lon;
    TextInputEditText confirm_location;

    Gson gson;
    Job currentJob;
    String jsonjob; String binid; String rfid; String type;
    String endpoint = "";
    TextInputEditText confirm_binid;
    TextInputEditText confirm_jobid;
    String loginkey;

    static final int CONFIRM_BACK_PRESSED = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        if(checkLogin(savedInstanceState)){
            gson = new Gson();

            //init GPS
            confirm_lat = (TextInputEditText) findViewById((R.id.confirm_input_lat));
            confirm_lon = (TextInputEditText) findViewById((R.id.confirm_input_lon));
            confirm_location = (TextInputEditText) findViewById(R.id.confirm_input_binlocation);

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            mLocationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);

            // Get the Intent that started this activity and extract the string
            if (savedInstanceState == null) {
                Bundle extras = getIntent().getExtras();
                if(extras == null) {
                    jsonjob = null;
                    binid = null;
                    rfid = null;
                    type = null;
                } else {
                    jsonjob = extras.getString("job");
                    binid = extras.getString("BinID");
                    rfid = extras.getString("RFID");
                    type = extras.getString("type");
                }
            } else {
                jsonjob = (String) savedInstanceState.getSerializable("job");
            }

            //set BinID
            if(binid != null){
                confirm_binid = (TextInputEditText) findViewById(R.id.confirm_input_binid);
                confirm_binid.setText(binid);
            }

            //set current job if there is one
            if(jsonjob != null){
                initJob(jsonjob);
                if(currentJob != null){
                    confirm_jobid = (TextInputEditText) findViewById(R.id.confirm_input_jobid);
                    String jid = String.valueOf(currentJob.getJobID());
                    confirm_jobid.setText(jid);
                }
            }
        }else{
            movetoLogin();
        }
    }

    public boolean checkLogin(Bundle savedInstanceState){
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                loginkey = null;
            } else {
                loginkey = extras.getString("loginkey");
            }
        } else {
            loginkey = (String) savedInstanceState.getSerializable("loginkey");
        }

        if(loginkey == null){
            return false;
        }else{
            return true;
        }
    }

    public void movetoLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    /****************** GPS METHODS *******************/
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE_FINE_LOCATION);

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE_COARSE_LOCATION);

            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {

            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.wtf("GPS CONNECTION: ", "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.wtf("GPS CONNECTION: ", "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE_FINE_LOCATION);

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_CODE_COARSE_LOCATION);

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onLocationChanged(Location location) {

        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mLocation = location;
    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                }
                return;
            }
            case PERMISSION_REQUEST_CODE_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                }
                return;
            }
        }
    }
    /****************** GPS *******************/

    //this override the implemented method from asyncTask
    @Override
    public void processFinish(String output){
        //Here you will receive the result fired from async class
        //of onPostExecute(result) method.
        processResult(output);
        //Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();

    }

    public void httpRequest(String[] request) {
        //execute the async task
        MyWebRequest asyncTask = new MyWebRequest(loginkey);
        asyncTask.delegate = this;

        //requestCmd (see MyWebRequest for list of cmds)
        asyncTask.execute(request);
    }

    public void processResult(String result){
        JsonParser parser = new JsonParser();
        JsonObject o = parser.parse(result).getAsJsonObject();

        if(endpoint == "GETREVERSEGEOCODE"){
            JsonElement je = o.get("GetReverseGeocodeResult");
            String address = je.getAsString();
            if(address != null) {
                Log.wtf("SHORT ADDRESS", address);
                confirm_location.setText(address);
            }
        }else if(endpoint == "UPDATEBINJOB"){
            JsonObject savebin = o.getAsJsonObject("UpdateBinJobResult");
            JsonElement je = savebin.get("Message");
            String res = je.getAsString();
            if(res.equals("OK")){
                Log.wtf("UPDATE BIN JOB", res);

                //move to end
                moveToComplete();
            }
        }
    }

    public void processJob(){
        if(currentJob != null){
            //call API to update job

            //TODO: handle job type

            //set InTransit to false
            currentJob.setInTransit(false);

            //set update end date
            currentJob.setEnd(getCurrentDate());

            //add EndPosition from device
            if(mLocation != null){
                Map<String, Double> ep = new HashMap<String, Double>();
                ep.put("Latitude", mLocation.getLatitude());
                ep.put("Longitude", mLocation.getLongitude());
                currentJob.setEndPosition(ep);
            }

            // JSON data structure
            JsonElement jsonElement = gson.toJsonTree(currentJob);
            JsonObject jsonObject = (JsonObject) jsonElement;
            // property removal
            jsonObject.remove("property");

            //remove StartPosition (not required by endpoint)
            jsonObject.remove("StartPosition");

            // serialization to String
            String javaObjectString = jsonObject.toString();
            Log.wtf("UPDATE JOB", javaObjectString);

            // http request to update bin job
            endpoint = "UPDATEBINJOB";
            String[] bin_data = {endpoint, javaObjectString};
            httpRequest(bin_data);
        }
    }

    public void initJob(String data){
        HashMap<String, Object> jobmap = new HashMap<String, Object>();
        jobmap = (HashMap<String, Object>) gson.fromJson(data, jobmap.getClass());
        Log.wtf("INIT JOB", jobmap.toString());
        currentJob = new Job(jobmap);
    }

    /******* DIALOG FOR CONFIRMATION + LISTENERS ********/
    public void confirmUpdate(View view){
        ConfirmDialogFragment confirmation = new ConfirmDialogFragment();
        confirmation.show(getFragmentManager(), "confirmation");
    }

    @Override
    public void confirmOk(DialogFragment dialog) {
        //save bin job if bin does not have one, otherwise proceed to confirmation
        processJob();

    }

    @Override
    public void confirmNOK(DialogFragment dialog) {
        //do nothing
    }
    /******* END DIALOG FOR CONFIRMATION + LISTENERS ********/

    public void updateLocation(View view){
        if(mLocation != null){
            confirm_lat.setText(String.valueOf(mLocation.getLatitude()));
            confirm_lon.setText(String.valueOf(mLocation.getLongitude()));

            //reverse geocode
            reverseLatLon(mLocation.getLatitude(), mLocation.getLongitude());
        }
    }

    protected String ObjectToJSON(Object ob){
        // JSON data structure
        JsonElement jsonElement = gson.toJsonTree(ob);
        JsonObject jsonObject = (JsonObject) jsonElement;

        // property removal
        jsonObject.remove("property");
        // serialization to String
        String javaObjectString = jsonObject.toString();
        Log.wtf("JSON STRING", javaObjectString);

        return javaObjectString;
    }

    public void reverseLatLon(Double lat, Double lon){
        Map<String, Double> ep = new HashMap<String, Double>();
        ep.put("Latitude", lat);
        ep.put("Longitude", lon);

        Map<String, Map<String, Double>> location = new HashMap<String, Map<String, Double>>();
        location.put("Position", ep);

        //convert to json
        String javaObjectString = ObjectToJSON(location);

        //call API to get string
        endpoint = "GETREVERSEGEOCODE";
        String[] bin_data = {endpoint, javaObjectString};
        httpRequest(bin_data);
    }

    private String getCurrentDate(){
        Calendar cal = Calendar.getInstance();
        Date currentLocalTime = cal.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String currDate = dateFormat.format(currentLocalTime);

        return currDate;
    }

    public void moveToComplete(){
        Intent intent = new Intent(getApplicationContext(), CompleteJobActivity.class);
        intent.putExtra("loginkey", loginkey);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        setResult(CONFIRM_BACK_PRESSED);
        super.onBackPressed();
    }
}

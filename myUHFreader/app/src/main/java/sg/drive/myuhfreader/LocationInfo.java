package sg.drive.myuhfreader;

public class LocationInfo {
    private String Address;
    private double Lat;
    private double Lng;

    public void setAddress(String address) {
        Address = address;
    }

    public String getAddress() {
        return Address;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLat() {
        return Lat;
    }

    public void setLng(double lng) {
        Lng = lng;
    }

    public double getLng() {
        return Lng;
    }
}

package sg.drive.myuhfreader;

import java.util.HashMap;

public class JobLocation
{
    private int JobID;
    private String Location;
    private HashMap<String, Double> Position;
    private String LastSeen;

    public int getJobID() { return JobID; }

    public void setJobID(int jobId) { JobID = jobId; }

    public String getLocation() { return Location; }

    public void setLocation(String location) { Location = location; }

    public HashMap<String, Double> getPosition() { return Position; }

    public void setPosition(HashMap<String, Double> position) { Position = position; }

    public String getLastSeen() { return LastSeen; }

    public void setLastSeen(String lastSeen) { LastSeen = lastSeen; }
}

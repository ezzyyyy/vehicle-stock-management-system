package sg.drive.myuhfreader;

import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Photos {
    private String Signature;
    private List<String> BinImages;

    public void setBinImages(List<String> binImages) {
        BinImages = binImages;
    }

    public List<String> getBinImages() {
        return BinImages;
    }

    public void setSignature(String signature) {
        Signature = signature;
    }

    public String getSignature() {
        return Signature;
    }
}
